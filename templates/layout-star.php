<?php

$class1 = '';
$class2 = '';
$class3 = '';
$class4 = '';
$class5 = '';
$class6 = '';
$class7 = '';
$class8 = '';


$producto = '';
if(isset($_GET['detproduct'])){
    $producto=$_GET['detproduct'];
}


if(isset($_GET['seccion'])){
    if($_GET['seccion'] == 'productos'){
        $class4 = 'active';
        $tittle="Productos";
        $name_page="Productos";

    }elseif($_GET['seccion'] == 'productos-detalles'){
        $class4 = 'active';
        $tittle="";
        $name_page="Productos";

    }elseif($_GET['seccion'] == 'posventa'){
        $class3 = 'active';
        $tittle="POSVENTA";
        $name_page="Posventa";

    }elseif($_GET['seccion'] == 'contacto'){
        $class2 = 'active';
        $tittle="FORMULARIO DE CONTACTO";
        $name_page="Formulario de contacto";

    }elseif($_GET['seccion'] == 'red-de-distribucion'){
        $class5 = 'active';
        $tittle="RED DE CONCESIONARIOS";
        $name_page="Red de concesionarios";

    }elseif($_GET['seccion'] == 'productos-detalles'){
        $tittle="Productos";
        $class5 = 'active';

    }elseif($_GET['seccion'] == 'test-drive') {
        $class6 = 'active';
        $tittle = "PRUEBA DE PRODUCTO";
        $name_page = "Test Drive";

    }elseif($_GET['seccion'] == 'noticias') {
        $class8 = 'active';
        $tittle = "NOTICIAS";
        $name_page = "noticias";

    }elseif($_GET['seccion'] == 'noticia') {
        $class8 = 'active';
        $tittle = "NOTICIA";
        $name_page = "noticia";

    }/*elseif($_GET['seccion'] == 'recall'){
        $class7 = 'active';
        $tittle="LLAMADO A CONSUMIDORES";
        $name_page="Recall";
    }*/
    if($producto=='7-5m'){
        $tittle="CAMIONES / CANTER / 7.5M";
    }elseif($producto=='7-5l'){
        $tittle="CAMIONES / CANTER / 7.5L";
    }elseif($producto=='8-2'){
        $tittle="CAMIONES / CANTER / 8.2";
    }elseif($producto=='10-4'){
        $tittle="CAMIONES / FI / 10.4";
    }elseif($producto=='10-4-2'){
        $tittle="CAMIONES / FI / 10.4.2";
    }elseif($producto=='7-5'){
        $tittle="CAMIONES / FUSO / 7.5";
    }elseif($producto=='9-0'){
        $tittle="CAMIONES / FUSO / 9.0";
    }
    // Noticias
    elseif($producto=='noticia1'){
        $tittle="Noticia 1 de prueba";
        $name_page = "Noticia 1 de prueba";
    }

}else{
    $name_page="FUSO";
    $class1 = 'active';
}


?>


<!doctype html>
<html lang="es">
<head>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php echo $name_page; ?></title>


    <!-- Bootstrap -->
    <link href="<?php echo $site_url; ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $site_url; ?>assets/css/main.css" rel="stylesheet">


</head>
<body >

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T43MTK7');</script>
<!-- End Google Tag Manager -->


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T43MTK7"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Lightning Bolt Begins -->
<script type="text/javascript">
    var lbTrans = '[TRANSACTION ID]';
    var lbValue = '[TRANSACTION VALUE]';
    var lbData = '[Attribute/Value Pairs for Custom Data]';
    var lb_rn = new String(Math.random()); var lb_rns = lb_rn.substring(2, 12);
    var boltProtocol = ('https:' == document.location.protocol) ? 'https://' : 'http://';
    try {
        var newScript = document.createElement('script');
        var scriptElement = document.getElementsByTagName('script')[0];
        newScript.type = 'text/javascript';
        newScript.id = 'lightning_bolt_' + lb_rns;
        newScript.src = boltProtocol + 'b3.mookie1.com/2/LB/' + lb_rns + '@x96?';
        scriptElement.parentNode.insertBefore(newScript, scriptElement);
        scriptElement = null; newScript = null;
    } catch (e) { }
</script>
<!-- Lightning Bolt Ends -->


<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-86472044-1', 'auto');
    ga('send', 'pageview');

</script>

<div class="page">
    <div class="container">
        <header class="clearfix">
            <div class="logo-fuso">
                <a href="<?php echo $site_url; ?>"><img src="<?php echo $site_url; ?>assets/img/common/logo.png" class="img-responsive"></a>
            </div>
            <div class="tittle-home hidden-xs">
                <?php if(!isset($tittle)) $tittle = ''; echo $tittle;?>
            </div>
            <div class="logo-day position-right hidden-sm">
                <img src="<?php echo $site_url; ?>assets/img/common/cadadiac.png" class="img-responsive">
            </div>
        </header>

        <div class="menu-principal col-md-1 hidden-xs hidden-sm clear ">
            <div  class="menu-main ">
                <ul>
                    <a href="<?php echo $link->ToSeccion('productos'); ?>"><li id="menu-produc" class="<?php echo $class4;?>"><img src="<?php echo $site_url; ?>assets/img/common/productos.png"></li></a>
                    <a href="<?php echo $link->ToSeccion('posventa'); ?>"><li class="li-postventa <?php echo $class3;?>"><img src="<?php echo $site_url; ?>assets/img/common/post-venta.png"></li></a>
                    <a href="#"><li id="menu-red-distri" class="<?php echo $class5;?>"><img src="<?php echo $site_url; ?>assets/img/common/red-de-distribucion.png"></li></a>
                    <a href="<?php echo $link->ToSeccion('contacto'); ?>" ><li class=" li-contacto <?php echo $class2;?>"><img src="<?php echo $site_url; ?>assets/img/common/contacto.png"></li></a>
                    <a href="<?php echo $link->ToSeccion('test-drive'); ?>" ><li class=" li-test-drive <?php echo $class6;?>"><img src="<?php echo $site_url; ?>assets/img/common/test-drive.png"></li></a>
                    <!--a href="<?php //echo $link->ToSeccion('noticias'); ?>" class="link-noticias" ><li class=" li-noticias <?php echo $class8;?>"><img src="<?php //echo $site_url; ?>assets/img/common/noticias.svg"><p>Noticias</p></li></a-->
                    <a href="http://fuso.com.co/recall/" ><li class=" li-test-drive <?php //echo $class7;?>"><img src="<?php echo $site_url; ?>assets/img/common/recall.png"></li></a>

            </div>
        </div>

        <div class="conten-sub-productos " id="effect"  style="display: none;">
            <ul class="sub-productos">
                <li>
                    <h2 class="aling-left-canter titulo-menu-n">CANTER</h2>
                    <hr class="hr-canter-top">
                    <ul class="menu-canter clear">
                        <a href="<?php echo $link->ToSubSeccion('productos-detalles','4-5M'); ?>"><li><h3 class="subtitulo-menu-n">4.5M</h3></li></a>
                        <a href="<?php echo $link->ToSubSeccion('productos-detalles','5-7M'); ?>"><li><h3 class="subtitulo-menu-n">5.7M</h3></li></a>
                        <a href="<?php echo $link->ToSubSeccion('productos-detalles','6-5M'); ?>"><li><h3 class="subtitulo-menu-n">6.5M</h3></li></a>
                        <a href="<?php echo $link->ToSubSeccion('productos-detalles','7-5M'); ?>"><li><h3 class="subtitulo-menu-n">7.5M</h3></li></a>
                        <a href="<?php echo $link->ToSubSeccion('productos-detalles','7-5L'); ?>"><li><h3 class="subtitulo-menu-n">7.5L</h3></li></a>
                        <a href="<?php echo $link->ToSubSeccion('productos-detalles','8-2'); ?>"><li><h3 class="subtitulo-menu-n">8.2</h3></li></a>
                    </ul>
                </li>
                <li>
                    <h2 class="titulo-menu-n aling-left-f1">FI</h2>
                    <hr class="hr-canter-top">
                    <ul class="menu-canter clear">
                        <a href="<?php echo $link->ToSubSeccion('productos-detalles','10-4'); ?>"><li><h3 class="subtitulo-menu-n">10.4 W4250</h3></li></a>
                        <a href="<?php echo $link->ToSubSeccion('productos-detalles','10-4-2'); ?>"><li><h3 class="subtitulo-menu-n">10.4 W4800</h3></li></a>
                    </ul>
                </li>
                <li>

                    <h2 class="titulo-menu-n aling-left-f1">FA</h2>


                    <hr class="hr-canter-top">
                    <ul class="menu-canter clear">
                        <a href="<?php echo $link->ToSubSeccion('productos-detalles','9-0'); ?>"><li><h3 class="subtitulo-menu-n">9.0</h3></li></a>
                    </ul>
                </li>
            </ul>

            <!--                <hr class="hr-canter-top">
                    <ul class="menu-canter clear">
                      <a href="<?php echo $link->ToSubSeccion('productos-detalles','7-5'); ?>"><li><img src="<?php echo $site_url; ?>assets/img/common/menu/7-5.png"></li></a>
                    </ul>
                  </li>
                </ul> -->


        </div>


        <div class="conten-sub-red-productos" id="effect-red" style="display: none;">
            <ul class="sub-red-produc">


                <a href="<?php echo $link->ToSubSeccion('red-de-distribucion','bogota'); ?>"><li>Bogotá <img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></li> </a>
                <a href="<?php echo $link->ToSubSeccion('red-de-distribucion','barranquilla'); ?>"><li>Barranquilla <img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></li></a>
                <a href="<?php echo $link->ToSubSeccion('red-de-distribucion','boyaca'); ?>"><li>Boyacá<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></li></a>
                <a href="<?php echo $link->ToSubSeccion('red-de-distribucion','bucaramanga'); ?>"><li>Bucaramanga<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></li></a>
                <a href="<?php echo $link->ToSubSeccion('red-de-distribucion','cali'); ?>"><li>Cali<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></li></a>
                <a href="<?php echo $link->ToSubSeccion('red-de-distribucion','cucuta'); ?>"><li>Cúcuta<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></li></a>
                <a href="<?php echo $link->ToSubSeccion('red-de-distribucion','ibague'); ?>"><li>Ibagué<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></li></a>
                <a href="<?php echo $link->ToSubSeccion('red-de-distribucion','medellin'); ?>"><li>Medellín<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></li></a>
                <a href="<?php echo $link->ToSubSeccion('red-de-distribucion','monteria'); ?>"><li>Montería<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></li></a>
                <a href="<?php echo $link->ToSubSeccion('red-de-distribucion','pereira'); ?>"><li>Pereira<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></li></a>
                <a href="<?php echo $link->ToSubSeccion('red-de-distribucion','villavicencio'); ?>"><li>Villavicencio<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></li></a>
                <!-- <a href="<?php echo $link->ToSubSeccion('red-de-distribucion','yopal'); ?>"><li>Yopal<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></li></a>-->



            </ul>
        </div>


        <!--Menu movil-->

        <div class="menu-movil visible-sm visible-xs" style="display: none">
            <button type="button" class="visible-sm visible-xs navbar-toggle" data-toggle="collapse" data-target="navbar-ex1-collapse">
                <span class="sr-only">Desplegar navegación</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <nav class="navbar navbar-inverse none-pading hidden-md hidden-lg" id="menumovil" style="display: none">



            <div class="container-fluid none-pading">

                <ul class="nav navbar-nav none-margin">

                    <li class="dropdown ">

                        <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo $link->ToSeccion('productos'); ?>">Productos
                            <span class="row-right"></span></a>
                        <ul class="dropdown-menu none-pading">
                            <li><a href="<?php echo $link->ToSubSeccion('productos-detalles','7-5L'); ?>">Canter 6.5</a></li>
                            <li><a href="<?php echo $link->ToSubSeccion('productos-detalles','7-5M'); ?>">Canter 7.5M</a></li>
                            <li><a href="<?php echo $link->ToSubSeccion('productos-detalles','7-5L'); ?>">Canter 7.5L</a></li>
                            <li><a href="<?php echo $link->ToSubSeccion('productos-detalles','7-5L'); ?>">New Canter 8.2L</a></li>
                            <!--                  <li><a href="<?php echo $link->ToSubSeccion('productos-detalles','8-2'); ?>">Canter 8.2</a></li>  -->

                            <li><a href="<?php echo $link->ToSubSeccion('productos-detalles','10-4'); ?>">Fl 10.4</a></li>
                            <li><a href="<?php echo $link->ToSubSeccion('productos-detalles','10-4-2'); ?>">Fl 10.4 V2</a></li>
                            <li><a href="<?php echo $link->ToSubSeccion('productos-detalles','7-5'); ?>">Fuso 7.5</a></li>
                            <li><a href="<?php echo $link->ToSubSeccion('productos-detalles','9-0'); ?>">Fuso 9.0</a></li>

                        </ul>
                    </li>
                    <li><a href="<?php echo $link->ToSeccion('posventa'); ?>">Posventa</a></li>


                    <li class="dropdown ">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Red de concesionarios
                            <span class="row-right"></span></a>
                        <ul class="dropdown-menu none-pading">
                            <li><a href="<?php echo $link->ToSubSeccion('red-de-distribucion','bogota'); ?>">Bogotá <img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></a></li>
                            <li><a href="<?php echo $link->ToSubSeccion('red-de-distribucion','barranquilla'); ?>">Barranquilla <img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></a></li>
                            <li><a href="<?php echo $link->ToSubSeccion('red-de-distribucion','boyaca'); ?>">Boyacá<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></a></li>
                            <li> <a href="<?php echo $link->ToSubSeccion('red-de-distribucion','bucaramanga'); ?>">Bucaramanga<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></a></li>
                            <li><a href="<?php echo $link->ToSubSeccion('red-de-distribucion','cali'); ?>">Cali<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></a></li>
                            <li><a href="<?php echo $link->ToSubSeccion('red-de-distribucion','cucuta'); ?>">Cúcuta<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></a></li>

                            <li><a href="<?php echo $link->ToSubSeccion('red-de-distribucion','ibague'); ?>">Ibagué<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></a></li>
                            <li><a href="<?php echo $link->ToSubSeccion('red-de-distribucion','medellin'); ?>">Medellín<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></a></li>
                            <li><a href="<?php echo $link->ToSubSeccion('red-de-distribucion','monteria'); ?>">Montería<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></a></li>
                            <li><a href="<?php echo $link->ToSubSeccion('red-de-distribucion','pereira'); ?>">Pereira<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></a></li>
                            <li> <a href="<?php echo $link->ToSubSeccion('red-de-distribucion','villavicencio'); ?>">Villavicencio<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></a></li>
                            <!-- <li><a href="<?php echo $link->ToSubSeccion('red-de-distribucion','yopal'); ?>">Yopal<img src="<?php echo $site_url; ?>assets/img/common/menu/flecha.png" class="row-menu"></a></li>-->
                        </ul>




                    </li>

                    <li><a href="<?php echo $link->ToSeccion('contacto'); ?>" >Contacto</a></li>

                    <li><a href="<?php echo $link->ToSeccion('test-drive'); ?>" >Test Drive</a></li>
                    <li><a href="http://fuso.com.co/recall/" >Llamado a consumidores</a></li>
                    <!--li><a href="<?php //echo $link->ToSeccion('noticias'); ?>" >Noticias</a></li-->



                </ul>
            </div>
        </nav>
        <!-- Fin Menu movil-->

        <!-- Espacio para el contenido-->
