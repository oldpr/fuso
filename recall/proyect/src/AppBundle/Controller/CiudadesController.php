<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Ciudades;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Ciudades controller.
 *
 */
class CiudadesController extends Controller
{
    /**
     * Lists all ciudades entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $ciudades = $em->getRepository('AppBundle:Ciudades')->findAll();

        return $this->render('ciudades/index.html.twig', array(
            'ciudades' => $ciudades,
        ));
    }

    /**
     * Creates a new ciudades entity.
     *
     */
    public function newAction(Request $request)
    {
        $ciudades = new Ciudades();
        $form = $this->createForm('AppBundle\Form\CiudadesType', $ciudades);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ciudades);
            $em->flush();

            return $this->redirectToRoute('admin_ciudades_edit', array('id' => $ciudades->getId()));
        }

        return $this->render('ciudades/new.html.twig', array(
            'ciudades' => $ciudades,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ciudades entity.
     *
     */
    public function showAction(Ciudades $ciudades)
    {
        $deleteForm = $this->createDeleteForm($ciudades);

        return $this->render('ciudades/show.html.twig', array(
            'ciudades' => $ciudades,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ciudades entity.
     *
     */
    public function editAction(Request $request, Ciudades $ciudades)
    {
        $deleteForm = $this->createDeleteForm($ciudades);
        $editForm = $this->createForm('AppBundle\Form\CiudadesType', $ciudades);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_ciudades_edit', array('id' => $ciudades->getId()));
        }

        return $this->render('ciudades/edit.html.twig', array(
            'ciudad' => $ciudades,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a ciudades entity.
     *
     */
    public function deleteAction(Request $request, Ciudades $ciudades)
    {
        $form = $this->createDeleteForm($ciudades);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ciudades);
            $em->flush();
        }

        return $this->redirectToRoute('admin_ciudades_index');
    }

    /**
     * Creates a form to delete a ciudades entity.
     *
     * @param Ciudades $ciudades The ciudades entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Ciudades $ciudades)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_ciudades_delete', array('id' => $ciudades->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
