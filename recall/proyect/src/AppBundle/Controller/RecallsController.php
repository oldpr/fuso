<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Recalls;
use DateTime;
use Doctrine\DBAL\Exception\ConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Date;

/**
 * Recalls controller.
 *
 */
class RecallsController extends Controller
{
    /**
     * Lists all recall entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $recalls = $em->getRepository('AppBundle:Recalls')->findAll();

        return $this->render('recalls/index.html.twig', array(
            'recalls' => $recalls,
        ));
    }

    /**
     * Creates a new recall entity.
     *
     */
    public function newAction(Request $request)
    {
        $recall = new Recalls();
        $recall->setFecha(new DateTime(date('Y-m-d')));
        $form = $this->createForm('AppBundle\Form\RecallsType', $recall);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // $file stores the uploaded PDF file
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            //$file = $recall->getImagen();
            $pdf = $recall->getLinkPdf();

            /*if(isset($file)){
                // Generate a unique name for the file before saving it
                $fileName = md5(uniqid()).'.'.$file->guessExtension();

                // Move the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('uploads_images_directory'),
                    $fileName
                    );
                // Update the 'brochure' property to store the PDF file name
                // instead of its contents
                $recall->setImagen($fileName);
            }*/
            if(isset($pdf)){
                $fileNamePdf = md5(uniqid()).'.'.$pdf->guessExtension();

                $pdf->move(
                    $this->getParameter('uploads_docs_directory'),
                    $fileNamePdf
                );

                $recall->setLinkPdf($fileNamePdf);
            }


            $em = $this->getDoctrine()->getManager();
            $em->persist($recall);
            $em->flush();

            return $this->redirectToRoute('admin_recalls_edit', array('id' => $recall->getId()));
        }

        return $this->render('recalls/new.html.twig', array(
            'recall' => $recall,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a recall entity.
     *
     */
    public function showAction(Recalls $recall)
    {
        $deleteForm = $this->createDeleteForm($recall);

        return $this->render('recalls/show.html.twig', array(
            'recall' => $recall,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing recall entity.
     *
     */
    public function editAction(Request $request, Recalls $recall)
    {
        $deleteForm = $this->createDeleteForm($recall);
        $editForm = $this->createForm('AppBundle\Form\RecallsType', $recall);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            // $file stores the uploaded PDF file
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            //$file = $recall->getImagen();
            $pdf = $recall->getLinkPdf();

            // Generate a unique name for the file before saving it
            /*if (isset($file)) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();

                // Move the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('uploads_images_directory'),
                    $fileName
                );

                // Update the 'brochure' property to store the PDF file name
                // instead of its contents
                $recall->setImagen($fileName);
            }else{
                $recall->setImagen($request->request->get('img'));
            }*/
            if (isset($pdf)) {
                $fileNamePdf = md5(uniqid()) . '.' . $pdf->guessExtension();
                $pdf->move(
                    $this->getParameter('uploads_docs_directory'),
                    $fileNamePdf
                );
                $recall->setLinkPdf($fileNamePdf);
            }else{
                $recall->setLinkPdf($request->request->get('pdf'));
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_recalls_edit', array('id' => $recall->getId()));
        }

        return $this->render('recalls/edit.html.twig', array(
            'recall' => $recall,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a recall entity.
     *
     */
    public function deleteAction(Request $request, Recalls $recall){
            $form = $this->createDeleteForm($recall);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                try{
                    $em->remove($recall);
                    $em->flush();
                }catch (ConstraintViolationException $e){
                    return $this->redirectToRoute('admin_recalls_edit', array('id' => $recall->getId(), 'error'=>'Este Recall NO se puede eliminar ya que  tiene códigos asociados'));
                }
            }
            return $this->redirectToRoute('admin_recalls_index');

    }

    /**
     * Creates a form to delete a recall entity.
     *
     * @param Recalls $recall The recall entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Recalls $recall)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_recalls_delete', array('id' => $recall->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
