<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Recalls
 */
class Recalls
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $codigo;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $imagen;

    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var integer
     */
    private $importancia;

    /**
     * @var string
     */
    private $informacion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $codigos;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->codigos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Recalls
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Recalls
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Recalls
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Recalls
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set importancia
     *
     * @param integer $importancia
     * @return Recalls
     */
    public function setImportancia($importancia)
    {
        $this->importancia = $importancia;

        return $this;
    }

    /**
     * Get importancia
     *
     * @return integer 
     */
    public function getImportancia()
    {
        return $this->importancia;
    }

    /**
     * Set informacion
     *
     * @param string $informacion
     * @return Recalls
     */
    public function setInformacion($informacion)
    {
        $this->informacion = $informacion;

        return $this;
    }

    /**
     * Get informacion
     *
     * @return string 
     */
    public function getInformacion()
    {
        return $this->informacion;
    }

    /**
     * Add codigos
     *
     * @param \AppBundle\Entity\Codigos $codigos
     * @return Recalls
     */
    public function addCodigo(\AppBundle\Entity\Codigos $codigos)
    {
        $this->codigos[] = $codigos;

        return $this;
    }

    /**
     * Remove codigos
     *
     * @param \AppBundle\Entity\Codigos $codigos
     */
    public function removeCodigo(\AppBundle\Entity\Codigos $codigos)
    {
        $this->codigos->removeElement($codigos);
    }

    /**
     * Get codigos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCodigos()
    {
        return $this->codigos;
    }

    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getImagen()
    {
        return $this->imagen;
    }
    /**
     * @var string
     */
    private $linkPdf;


    /**
     * Set linkPdf
     *
     * @param string $linkPdf
     * @return Recalls
     */
    public function setLinkPdf($linkPdf)
    {
        $this->linkPdf = $linkPdf;

        return $this;
    }

    /**
     * Get linkPdf
     *
     * @return string 
     */
    public function getLinkPdf()
    {
        return $this->linkPdf;
    }
}
