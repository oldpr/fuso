<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecallsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*->add('imagen', 'file', array(
                'label'=> 'Seleccionar imagen',
                'data_class' => null,
                'required' => false,
                'label_attr'=> array('class'=>'col-md-2 control-label'),
                'attr'=> array('class'=> 'col-md-12 form-control'),
            ))*/
            ->add('codigo', 'text', array(
                'label'=>'Código',
                'required' => true,
                'label_attr'=> array('class'=>'col-md-2 control-label'),
                'attr'=> array('class'=> 'col-md-12 form-control'),
            ))
            ->add('nombre', 'text', array(
                'label'=>'Nombre',
                'required' => true,
                'label_attr'=> array('class'=>'col-md-2 control-label'),
                'attr'=> array('class'=> 'col-md-12 form-control'),
            ))
            /*->add('descripcion','textarea', array(
                'label'=>'Descripción',
                'required' => false,
                'label_attr'=> array('class'=>'col-md-12 control-label'),
                'attr' => array('class' => 'form-control summernote')
            ))*/
            ->add('informacion','textarea', array(
                'label'=>'Información final',
                'required' => false,
                'label_attr'=> array('class'=>'col-md-12 control-label'),
                'attr' => array('class' => 'form-control summernote')
            ))
            ->add('fecha', 'date', array(
                'label'=>'Fecha',
                'required' => false,
                'label_attr'=> array('class'=>'col-md-2 control-label'),
                'attr'=> array('class'=> 'col-md-12 panel'),
            ))
            ->add('linkPdf', 'file', array(
                'label'=> 'Seleccionar PDF',
                'data_class' => null,
                'required' => false,
                'label_attr'=> array('class'=>'col-md-2 control-label'),
                'attr'=> array('class'=> 'col-md-12 form-control'),
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Recalls'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_recalls';
    }


}
