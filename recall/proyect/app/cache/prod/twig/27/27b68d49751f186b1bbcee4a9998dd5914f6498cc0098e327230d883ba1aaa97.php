<?php

/* AppBundle:Default:datosEnviados.html.twig */
class __TwigTemplate_3129f3a2f8360fd32765c560b14c168014ca2805c3755a190c39f2583874ca5a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Default:datosEnviados.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "
    <h3>EL CÓDIGO APLICA</h3>
    ";
        // line 5
        echo $this->getAttribute($this->getAttribute(($context["codigo"] ?? null), "recall", array()), "informacion", array());
        echo "
    <div class=\"botones\" style=\"margin-top: 30px\">
        <div class=\"col-md-6\">
            <a>Red de concesionarios</a>
        </div>
        <div class=\"col-md-6\">
            <a>Ir al inicio</a>
        </div>
    </div>


";
    }

    public function getTemplateName()
    {
        return "AppBundle:Default:datosEnviados.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 5,  31 => 3,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:Default:datosEnviados.html.twig", "/home/120818.cloudwaysapps.com/vqpqagqqyk/public_html/recall/proyect/src/AppBundle/Resources/views/Default/datosEnviados.html.twig");
    }
}
