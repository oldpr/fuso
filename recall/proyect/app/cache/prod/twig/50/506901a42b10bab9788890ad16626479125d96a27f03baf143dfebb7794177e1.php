<?php

/* AdminBundle:Security:login.html.twig */
class __TwigTemplate_ed14a4ebad5cab50a2970a68d7b2a2b427b6302ee710159e65d2b8aac25476dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\" />
    <title>Login Dashboard Daimler Colombia</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" />
    <meta content=\"Login del Dash y cms del portal oficial de Daimler Colombia.\" name=\"description\" />
    <meta content=\"Coderthemes\" name=\"author\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />

    <link rel=\"shortcut icon\" href=' ";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/favicon.ico"), "html", null, true);
        echo " '>

    <link href=' ";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/bootstrap.min.css"), "html", null, true);
        echo " ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/core.css"), "html", null, true);
        echo " ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' ";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/icons.css"), "html", null, true);
        echo " ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' ";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/components.css"), "html", null, true);
        echo " ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' ";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/pages.css"), "html", null, true);
        echo " ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' ";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/menu.css"), "html", null, true);
        echo " ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' ";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/responsive.css"), "html", null, true);
        echo " ' rel=\"stylesheet\" type=\"text/css\">

    <script src=' ";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/modernizr.min.js"), "html", null, true);
        echo " '></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js\"></script>
    <![endif]-->


</head>
<body>


<div class=\"wrapper-page\">
    <div class=\"panel panel-color panel-primary panel-pages\">
        <div class=\"panel-heading bg-img\">
            <div class=\"bg-overlay\"></div>
            <h3 class=\"text-center m-t-10 text-white\"> Sign In to <strong>Admin to <a class=\"text text-info\" target=\"_blank\" href=\"";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">Daimler Colombia</a></strong> </h3>
        </div>


        <div class=\"panel-body\">
            ";
        // line 44
        if (($context["error"] ?? null)) {
            // line 45
            echo "                <div class=\"alert alert-danger alert-dismissible fade in\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
                    ";
            // line 47
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? null), "messageKey", array()), $this->getAttribute(($context["error"] ?? null), "messageData", array()), "security"), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 50
        echo "
            <form class=\"form-horizontal m-t-20\" action=\"";
        // line 51
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"post\">

                <div class=\"form-group\">
                    <div class=\"col-xs-12\">
                        <input class=\"form-control input-lg\" type=\"text\" name=\"_username\" value=\"";
        // line 55
        echo twig_escape_filter($this->env, ($context["last_username"] ?? null), "html", null, true);
        echo "\" required=\"\" placeholder=\"Username\">
                    </div>
                </div>

                <div class=\"form-group\">
                    <div class=\"col-xs-12\">
                        <input class=\"form-control input-lg\" type=\"password\" name=\"_password\" required=\"\" placeholder=\"Password\">
                    </div>
                </div>

                <div class=\"form-group text-center m-t-40\">
                    <div class=\"col-xs-12\">
                        <button class=\"btn btn-primary btn-lg w-lg waves-effect waves-light\" type=\"submit\">Log In</button>
                    </div>
                </div>
            </form>
            <a href=\"";
        // line 71
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\" class=\"align-right btn btn-flat waves-effect waves-light btn-xs\">Ir al sitio</a>

        </div>

    </div>
</div>


<script>
    var resizefunc = [];
</script>

<!-- Main  -->
<script src=' ";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.min.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/bootstrap.min.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/detect.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/fastclick.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.slimscroll.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.blockUI.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/waves.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/wow.min.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.nicescroll.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.scrollTo.min.js"), "html", null, true);
        echo " '></script>

<script src=' ";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.app.js"), "html", null, true);
        echo " '></script>

</body>
</html>";
    }

    public function getTemplateName()
    {
        return "AdminBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 95,  187 => 93,  183 => 92,  179 => 91,  175 => 90,  171 => 89,  167 => 88,  163 => 87,  159 => 86,  155 => 85,  151 => 84,  135 => 71,  116 => 55,  109 => 51,  106 => 50,  100 => 47,  96 => 45,  94 => 44,  86 => 39,  65 => 21,  60 => 19,  56 => 18,  52 => 17,  48 => 16,  44 => 15,  40 => 14,  36 => 13,  31 => 11,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AdminBundle:Security:login.html.twig", "/home/120818.cloudwaysapps.com/vqpqagqqyk/public_html/recall/proyect/src/AdminBundle/Resources/views/Security/login.html.twig");
    }
}
