<?php

/* base.html.twig */
class __TwigTemplate_574b164d293a0c64c40153e51e6157bb26098f1f97b083dafe4fda3b9cf8d30b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'titlePag' => array($this, 'block_titlePag'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"es\">
<head>


    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
    <title>";
        // line 8
        $this->displayBlock('title', $context, $blocks);
        echo "</title>


    <!-- Bootstrap -->
    <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/css/main.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    ";
        // line 14
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 15
        echo "
</head>
<body >

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T43MTK7');</script>
<!-- End Google Tag Manager -->


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-T43MTK7\"
                  height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Lightning Bolt Begins -->
<script type=\"text/javascript\">
    var lbTrans = '[TRANSACTION ID]';
    var lbValue = '[TRANSACTION VALUE]';
    var lbData = '[Attribute/Value Pairs for Custom Data]';
    var lb_rn = new String(Math.random()); var lb_rns = lb_rn.substring(2, 12);
    var boltProtocol = ('https:' == document.location.protocol) ? 'https://' : 'http://';
    try {
        var newScript = document.createElement('script');
        var scriptElement = document.getElementsByTagName('script')[0];
        newScript.type = 'text/javascript';
        newScript.id = 'lightning_bolt_' + lb_rns;
        newScript.src = boltProtocol + 'b3.mookie1.com/2/LB/' + lb_rns + '@x96?';
        scriptElement.parentNode.insertBefore(newScript, scriptElement);
        scriptElement = null; newScript = null;
    } catch (e) { }
</script>
<!-- Lightning Bolt Ends -->


<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-86472044-1', 'auto');
    ga('send', 'pageview');

</script>

<div class=\"page\">
    <div class=\"container\">
        <header class=\"clearfix\">
            <div class=\"logo-fuso\">
                <a href=\"http://www.fuso.com.co\"><img src=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/logo.png"), "html", null, true);
        echo "\" class=\"img-responsive\"></a>
            </div>
            <div class=\"tittle-home hidden-xs\">
                ";
        // line 71
        $this->displayBlock('titlePag', $context, $blocks);
        // line 72
        echo "            </div>
            <div class=\"logo-day position-right hidden-sm\">
                <img src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/cadadiac.png"), "html", null, true);
        echo "\" class=\"img-responsive\">
            </div>
        </header>

        <div class=\"menu-principal col-md-1 hidden-xs hidden-sm clear \">
            <div  class=\"menu-main \">
                <ul>
                    <a href=\"http://fuso.com.co/s-productos/\"><li id=\"menu-produc\" class=\"\"><img src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/productos.png"), "html", null, true);
        echo "\"> </li></a>
                    <a href=\"http://fuso.com.co/s-posventa/\"><li class=\"li-postventa \"><img src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/post-venta.png"), "html", null, true);
        echo "\"></li></a>
                    <a href=\"#\"><li id=\"menu-red-distri\" class=\"\"><img src=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/red-de-distribucion.png"), "html", null, true);
        echo "\"></li></a>
                    <a href=\"http://fuso.com.co/s-contacto/\" ><li class=\" li-contacto \"><img src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/contacto.png"), "html", null, true);
        echo "\"></li></a>
                    <a href=\"http://fuso.com.co/s-test-drive/\" ><li class=\" li-test-drive \"><img src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/test-drive.png"), "html", null, true);
        echo "\"></li></a>
                    <a href=\"";
        // line 86
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\" ><li class=\" li-test-drive active\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/recall.png"), "html", null, true);
        echo "\"></li></a>

            </div>
        </div>

        <div class=\"conten-sub-productos \" id=\"effect\"  style=\"display: none;\">
            <ul class=\"sub-productos\">
                <li>
                    <h2 class=\"aling-left-canter titulo-menu-n\">CANTER</h2>
                    <hr class=\"hr-canter-top\">
                    <ul class=\"menu-canter clear\">
                        <a href=\"http://fuso.com.co/s-productos-detalles/7-5m/\"><li><h3 class=\"subtitulo-menu-n\">7.5M</h3></li></a>
                        <a href=\"http://fuso.com.co/s-productos-detalles/7-5l/\"><li><h3 class=\"subtitulo-menu-n\">7.5L</h3></li></a>
                        <a href=\"http://fuso.com.co/s-productos-detalles/8-2/\"><li><h3 class=\"subtitulo-menu-n\">8.2</h3></li></a>
                    </ul>
                </li>
                <li>
                    <h2 class=\"titulo-menu-n aling-left-f1\">FI</h2>
                    <hr class=\"hr-canter-top\">
                    <ul class=\"menu-canter clear\">
                        <a href=\"http://fuso.com.co/s-productos-detalles/10-4/\"><li><h3 class=\"subtitulo-menu-n\">10.4 W4250</h3></li></a>
                        <a href=\"http://fuso.com.co/s-productos-detalles/10-4-2/\"><li><h3 class=\"subtitulo-menu-n\">10.4 W4800</h3></li></a>
                    </ul>
                </li>
            </ul>
        </div>


        <div class=\"conten-sub-red-productos\" id=\"effect-red\" style=\"display: none;\">
            <ul class=\"sub-red-produc\">


                <a href=\"http://fuso.com.co/s-red-de-distribucion/bogota/\"       ><li>Bogotá <img src=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></li> </a>
                <a href=\"http://fuso.com.co/s-red-de-distribucion/barranquilla/\" ><li>Barranquilla <img src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></li></a>
                <a href=\"http://fuso.com.co/s-red-de-distribucion/boyaca/\"       ><li>Boyacá<img src=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></li></a>
                <a href=\"http://fuso.com.co/s-red-de-distribucion/bucaramanga/\"  ><li>Bucaramanga<img src=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></li></a>
                <a href=\"http://fuso.com.co/s-red-de-distribucion/cali/\"         ><li>Cali<img src=\"";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></li></a>
                <a href=\"http://fuso.com.co/s-red-de-distribucion/cucuta/\"       ><li>Cúcuta<img src=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></li></a>
                <a href=\"http://fuso.com.co/s-red-de-distribucion/ibague/\"       ><li>Ibagué<img src=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></li></a>
                <a href=\"http://fuso.com.co/s-red-de-distribucion/medellin/\"     ><li>Medellín<img src=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></li></a>
                <a href=\"http://fuso.com.co/s-red-de-distribucion/monteria/\"     ><li>Montería<img src=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></li></a>
                <a href=\"http://fuso.com.co/s-red-de-distribucion/pereira/\"      ><li>Pereira<img src=\"";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></li></a>
                <a href=\"http://fuso.com.co/s-red-de-distribucion/villavicencio/\"><li>Villavicencio<img src=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></li></a>
                ";
        // line 130
        echo "


            </ul>
        </div>


        <!--Menu movil-->

        <div class=\"menu-movil visible-sm visible-xs\" style=\"display: none\">
            <button type=\"button\" class=\"visible-sm visible-xs navbar-toggle\" data-toggle=\"collapse\" data-target=\"navbar-ex1-collapse\">
                <span class=\"sr-only\">Desplegar navegación</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
        </div>

        <nav class=\"navbar navbar-inverse none-pading hidden-md hidden-lg\" id=\"menumovil\" style=\"display: none\">



            <div class=\"container-fluid none-pading\">

                <ul class=\"nav navbar-nav none-margin\">

                    <li class=\"dropdown \">

                        <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"http://fuso.com.co/s-productos/\">Productos
                            <span class=\"row-right\"></span></a>
                        <ul class=\"dropdown-menu none-pading\">
                            <li><a href=\"http://fuso.com.co/s-productos-detalles/7-5m/\">Canter 7.5M</a></li>
                            <li><a href=\"http://fuso.com.co/s-productos-detalles/7-5l/\">Canter 7.5L</a></li>
                            <li><a href=\"http://fuso.com.co/s-productos-detalles/8-2/\">Canter 8.2</a></li>

                            <li><a href=\"http://fuso.com.co/s-productos-detalles/10-4/\">10.4 W4250</a></li>
                            <li><a href=\"http://fuso.com.co/s-productos-detalles/10-4-2/\">10.4 W4800</a></li>

                        </ul>
                    </li>
                    <li><a href=\"http://fuso.com.co/s-posventa/\">Posventa</a></li>


                    <li class=\"dropdown \">
                        <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">Red de concesionarios
                            <span class=\"row-right\"></span></a>
                        <ul class=\"dropdown-menu none-pading\">
                            <li><a href=\"http://fuso.com.co/s-red-de-distribucion/bogota/\"       >Bogotá <img src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></a></li>
                            <li><a href=\"http://fuso.com.co/s-red-de-distribucion/barranquilla/\" >Barranquilla <img src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></a></li>
                            <li><a href=\"http://fuso.com.co/s-red-de-distribucion/boyaca/\"       >Boyacá<img src=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></a></li>
                            <li><a href=\"http://fuso.com.co/s-red-de-distribucion/bucaramanga/\"  >Bucaramanga<img src=\"";
        // line 181
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></a></li>
                            <li><a href=\"http://fuso.com.co/s-red-de-distribucion/cali/\"         >Cali<img src=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></a></li>
                            <li><a href=\"http://fuso.com.co/s-red-de-distribucion/cucuta/\"       >Cúcuta<img src=\"";
        // line 183
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></a></li>
                            <li><a href=\"http://fuso.com.co/s-red-de-distribucion/ibague/\"       >Ibagué<img src=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></a></li>
                            <li><a href=\"http://fuso.com.co/s-red-de-distribucion/medellin/\"     >Medellín<img src=\"";
        // line 185
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></a></li>
                            <li><a href=\"http://fuso.com.co/s-red-de-distribucion/monteria/\"     >Montería<img src=\"";
        // line 186
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></a></li>
                            <li><a href=\"http://fuso.com.co/s-red-de-distribucion/pereira/\"      >Pereira<img src=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></a></li>
                            <li><a href=\"http://fuso.com.co/s-red-de-distribucion/villavicencio/\">Villavicencio<img src=\"";
        // line 188
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></a></li>
                            <!-- <li><a href=\"<?php echo \$link->ToSubSeccion('red-de-distribucion','yopal'); ?>\">Yopal<img src=\"";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/menu/flecha.png"), "html", null, true);
        echo "\" class=\"row-menu\"></a></li>-->
                        </ul>




                    </li>

                    <li><a href=\"http://fuso.com.co/s-contacto/\" >Contacto</a></li>

                    <li><a href=\"http://fuso.com.co/s-test-drive/\" >Test Drive</a></li>



                </ul>
            </div>
        </nav>
        <!-- Fin Menu movil-->

        <!-- Espacio para el contenido-->
        <div class=\"container-recall col-md-11\">
            <div class=\"col-md-12 img-contacto\">
                <img src=\"";
        // line 211
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/recall/home-recall.jpg"), "html", null, true);
        echo "\" class=\"img-responsive\">
            </div>
            <div class=\"contenido\">
                <h1>CAMPAÑA DE SEGURIDAD</h1>

                <div class=\"margenes-anchas text-center\">
                    ";
        // line 217
        $this->displayBlock('body', $context, $blocks);
        // line 218
        echo "                </div>

            </div>
        </div>
        <footer class=\"clearfix col-md-12 col-md-offset-0\" >
            <span class=\"tyc\"><a href=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/pdf/Terminos-y-Condiciones-site-FUSO.pdf"), "html", null, true);
        echo "\" target=\"_blank\">Políticas de privacidad y términos y condiciones.</a></span>
            | <span class=\"tyc\"><a href=\"http://fuso.com.co/s-garantia/\">Garantía.</a></span>
        </footer>

    </div>



</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src=\"";
        // line 235
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/js/jquery-1.12.4.min.js"), "html", null, true);
        echo "\"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src=\"";
        // line 237
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 238
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/js/jquery.sudoSlider.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"https://code.jquery.com/ui/1.12.0/jquery-ui.js\"></script>
<script src=\"";
        // line 240
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/js/main.js"), "html", null, true);
        echo "\"></script>
";
        // line 241
        $this->displayBlock('javascripts', $context, $blocks);
        // line 242
        echo "</body>
</html>
\t";
    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
        echo "Recall | Fuso";
    }

    // line 14
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 71
    public function block_titlePag($context, array $blocks = array())
    {
    }

    // line 217
    public function block_body($context, array $blocks = array())
    {
    }

    // line 241
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  427 => 241,  422 => 217,  417 => 71,  412 => 14,  406 => 8,  400 => 242,  398 => 241,  394 => 240,  389 => 238,  385 => 237,  380 => 235,  365 => 223,  358 => 218,  356 => 217,  347 => 211,  322 => 189,  318 => 188,  314 => 187,  310 => 186,  306 => 185,  302 => 184,  298 => 183,  294 => 182,  290 => 181,  286 => 180,  282 => 179,  278 => 178,  228 => 130,  224 => 128,  220 => 127,  216 => 126,  212 => 125,  208 => 124,  204 => 123,  200 => 122,  196 => 121,  192 => 120,  188 => 119,  184 => 118,  147 => 86,  143 => 85,  139 => 84,  135 => 83,  131 => 82,  127 => 81,  117 => 74,  113 => 72,  111 => 71,  105 => 68,  50 => 15,  48 => 14,  44 => 13,  40 => 12,  33 => 8,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html.twig", "/home/120818.cloudwaysapps.com/vqpqagqqyk/public_html/recall/proyect/app/Resources/views/base.html.twig");
    }
}
