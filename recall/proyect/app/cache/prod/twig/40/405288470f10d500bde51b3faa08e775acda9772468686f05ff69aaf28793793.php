<?php

/* AppBundle:Default:buscarCodigo.html.twig */
class __TwigTemplate_1b0e0bd4fa33df2ddd2ab087a3bf4901e6b596f8ce50551a7c3ea0c96939b3f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Default:buscarCodigo.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        if (array_key_exists("codigos", $context)) {
            // line 5
            echo "

        <h3>EL CÓDIGO APLICA</h3>
        <p>Código <b>";
            // line 8
            echo twig_escape_filter($this->env, ($context["codigo"] ?? null), "html", null, true);
            echo ",</b> aplica para la campaña de seguridad ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["codigos"] ?? null), 0, array(), "array"), "recall", array()), "nombre", array()), "html", null, true);
            echo ", ingrese sus datos de contacto para obtener más información.</p>


        <form action=\"";
            // line 11
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("guardarRegistro");
            echo "\" method=\"post\">
            <input type=\"hidden\" name=\"codigo\" value=\"";
            // line 12
            echo twig_escape_filter($this->env, ($context["codigo"] ?? null), "html", null, true);
            echo "\">

            <div class=\"row\">
                <div class=\"col-md-6\">
                    <input type=\"text\" name=\"nombres\" class=\"form-control\" style=\"height:45px;min-height:45px;border-bottom-color:#ccc;border-bottom-width:1px;\" placeholder=\"Nombres\" required>
                </div>
                <div class=\"col-md-6\">
                    <input type=\"text\" name=\"apellidos\" class=\"form-control\" style=\"height:45px;min-height:45px;border-bottom-color:#ccc;border-bottom-width:1px;\" placeholder=\"Apellidos\" required>
                </div>
            </div>
            <br>
            <div class=\"row\">
                <div class=\"col-md-6\">
                    <input type=\"email\" name=\"correo\" class=\"form-control\" style=\"height:45px;min-height:45px;border-bottom-color:#ccc;border-bottom-width:1px;\" placeholder=\"Correo electrónico\" required>
                </div>
                <div class=\"col-md-6\">
                    <input type=\"text\" name=\"telefono\" class=\"form-control\" style=\"height:45px;min-height:45px;border-bottom-color:#ccc;border-bottom-width:1px;\" placeholder=\"Télefono de contacto\" required>
                    <input type=\"hidden\" name=\"misRegistros\" value=\"";
            // line 29
            echo twig_escape_filter($this->env, twig_jsonencode_filter(($context["misRegistros"] ?? null)), "html", null, true);
            echo "\" >

                </div>
            </div>
            <input type=\"submit\" class=\"btn btn-negro form-control\" value=\"Ver información\">
        </form>

    ";
        } else {
            // line 37
            echo "
        <h3>LLAMADOS DE SEGURIDAD</h3>
        <p>El código <b>";
            // line 39
            echo twig_escape_filter($this->env, ($context["codigo"] ?? null), "html", null, true);
            echo "</b>, no aplica para alguna de las campañas de seguridad vigentes de Freightliner Colombia Rectifique el código o intente nuevamente.</p>

        <form action=\"";
            // line 41
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("buscarCodigo");
            echo "\" method=\"post\">
            <input type=\"text\" name=\"codigo\" class=\"form-control\" placeholder=\"Ingrese su código VIN o FIN\" required>
            <div class=\"botones\">
                <div class=\"col-md-6\"><input type=\"submit\" class=\"btn btn-negro\" style=\"width:90%;\" value=\"VOLVER A BUSCAR\"></div>
                <div class=\"col-md-6\"><a href=\"http://www.daimler.com.co/\" class=\"btn btn-negro\" >IR AL SITIO WEB</a></div>
            </div>
        </form>





    ";
        }
    }

    public function getTemplateName()
    {
        return "AppBundle:Default:buscarCodigo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 41,  86 => 39,  82 => 37,  71 => 29,  51 => 12,  47 => 11,  39 => 8,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:Default:buscarCodigo.html.twig", "/home/120818.cloudwaysapps.com/vqpqagqqyk/public_html/recall/proyect/src/AppBundle/Resources/views/Default/buscarCodigo.html.twig");
    }
}
