<?php

/* AdminBundle:Default:registros.html.twig */
class __TwigTemplate_5323035490b4cb1d8198dcb7b6e2526347f0551a1b3eb9f94e78c5a8f440564b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", "AdminBundle:Default:registros.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["nomPagina"] = "Registros";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? null), "html", null, true);
    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        // line 6
        echo "    <li class=\"active\">";
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? null), "html", null, true);
        echo "</li>
";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Registros</h3>
                </div>
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Codigo VIN</th>
                                    <th>Fecha</th>
                                    <th>Nombres</th>
                                    <th>Correo</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>


                                <tbody>
                                ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["registros"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["registro"]) {
            // line 34
            echo "                                    <tr>
                                        <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["registro"], "id", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["registro"], "codigo", array()), "codigo", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 37
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["registro"], "fecha", array())), "html", null, true);
            echo "</td>
                                        <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["registro"], "nombres", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["registro"], "correo", array()), "html", null, true);
            echo "</td>
                                        <td>
                                            ";
            // line 41
            if ((((($this->getAttribute($context["registro"], "nombres", array()) != "") || ($this->getAttribute($context["registro"], "apellidos", array()) != "")) || ($this->getAttribute($context["registro"], "telefono", array()) != "")) || ($this->getAttribute($context["registro"], "correo", array()) != ""))) {
                // line 42
                echo "                                                <a class=\"btn btn-primary\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_registro", array("idRegistro" => $this->getAttribute($context["registro"], "id", array()))), "html", null, true);
                echo "\">Ver Formulario</a>
                                            ";
            } else {
                // line 44
                echo "                                                No envio Formulario
                                            ";
            }
            // line 46
            echo "                                        </td>
                                    </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['registro'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- End Row -->
";
    }

    public function getTemplateName()
    {
        return "AdminBundle:Default:registros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 49,  117 => 46,  113 => 44,  107 => 42,  105 => 41,  100 => 39,  96 => 38,  92 => 37,  88 => 36,  84 => 35,  81 => 34,  77 => 33,  52 => 10,  49 => 9,  42 => 6,  39 => 5,  33 => 4,  29 => 1,  27 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AdminBundle:Default:registros.html.twig", "/home/120818.cloudwaysapps.com/vqpqagqqyk/public_html/recall/proyect/src/AdminBundle/Resources/views/Default/registros.html.twig");
    }
}
