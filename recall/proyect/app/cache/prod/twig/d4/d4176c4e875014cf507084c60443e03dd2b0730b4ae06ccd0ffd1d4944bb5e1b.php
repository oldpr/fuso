<?php

/* AdminBundle:Default:subirCodigosRecall.html.twig */
class __TwigTemplate_2e93ddcc4d900b8fa5fbc769964ea07badf388cf48378488c8505ab58310588d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", "AdminBundle:Default:subirCodigosRecall.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["nomPagina"] = (("Subir códigos a Recoll \"" . $this->getAttribute(($context["entidad"] ?? null), "nombre", array())) . "\"");
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? null), "html", null, true);
    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_index");
        echo "\">Recalls</a></li>
    <li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_edit", array("id" => $this->getAttribute(($context["entidad"] ?? null), "id", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["entidad"] ?? null), "nombre", array()), "html", null, true);
        echo "</a></li>
    <li class=\"active\">";
        // line 8
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? null), "html", null, true);
        echo "</li>
";
    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        // line 12
        echo "    ";
        if (array_key_exists("aux", $context)) {
            // line 13
            echo "        ";
            echo twig_escape_filter($this->env, ($context["aux"] ?? null), "html", null, true);
            echo "
    ";
        }
        // line 15
        echo "    <!-- container -->
    ";
        // line 16
        if ((array_key_exists("error", $context) && (($context["error"] ?? null) != null))) {
            // line 17
            echo "        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            ERROR: ";
            // line 19
            echo twig_escape_filter($this->env, ($context["error"] ?? null), "html", null, true);
            echo ".
        </div>
    ";
        }
        // line 22
        echo "    <div class=\"alert alert-warning \">
        <b>Advertencia!</b> Tenga en cuenta que al cargar un nuevo archivo los codigos VIN actuales se borraran de forma permanente.
    </div>
    ";
        // line 25
        if (( !array_key_exists("success", $context) || (($context["success"] ?? null) == false))) {
            // line 26
            echo "        <div class=\"row\">
            <!-- Basic example -->
            <div class=\"col-md-12\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\"><h3 class=\"panel-title\">Paso 1 - Cargue el archivo CSV o TXT</h3></div>
                    <div class=\"panel-body\">
                        <form method=\"post\" enctype=\"multipart/form-data\" action=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("subirCodigosRecall", array("idRecall" => $this->getAttribute(($context["entidad"] ?? null), "id", array()))), "html", null, true);
            echo "\"  role=\"form\">
                            <div class=\"form-group\">
                                <!-- MAX_FILE_SIZE debe preceder al campo de entrada del fichero -->
                                <input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"20000\" />
                                <label for=\"archivo\">Ingrese el archivo</label>
                                <input type=\"file\" name=\"fichero_usuario\" accept=\"text/*, .csv\"  class=\"form-control\" id=\"archivo\" required>
                            </div>
                            <div class=\"form-group\">
                                <div class=\"radio radio-info\">
                                    <input name=\"caracter\" value=\",\" id=\"radio1\" type=\"radio\" checked>
                                    <label for=\"radio1\">
                                        CSV separado por comas ( , )
                                    </label>
                                    <br>
                                    <input name=\"caracter\" value=\";\" id=\"radio2\" type=\"radio\">
                                    <label for=\"radio2\">
                                        CSV separado por punto y coma ( ; )
                                    </label>
                                </div>
                            </div>
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Cargar CSV</button>
                        </form>
                    </div><!-- panel-body -->
                </div> <!-- panel -->
            </div> <!-- col-->
        </div>
    ";
        }
        // line 59
        echo "    ";
        if ((array_key_exists("paso2", $context) && ($context["paso2"] ?? null))) {
            // line 60
            echo "        <div class=\"row\">
            <div class=\"col-md-12\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        <h3 class=\"panel-title\">Paso 2 - Verifique la información</h3>
                    </div>
                    <div class=\"panel-body\">
                        <div class=\"row\">
                            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                                <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                    <thead>
                                    <tr>
                                        <th>Codigo</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                    ";
            // line 78
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["codigos"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["codigo"]) {
                if ((($context["codigos"] ?? null) != null)) {
                    // line 79
                    echo "                                        <tr>
                                                <td>";
                    // line 80
                    echo twig_escape_filter($this->env, $context["codigo"], "html", null, true);
                    echo "</td>
                                        </tr>
                                    ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['codigo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 83
            echo "                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- End Row -->

        ";
            // line 94
            if (( !array_key_exists("success", $context) || (($context["success"] ?? null) == false))) {
                // line 95
                echo "            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h3 class=\"panel-title\">Paso 3 - Actualice los códigos</h3>
                        </div>
                        <form  method=\"post\" action=\"";
                // line 101
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("subirCodigosRecall", array("idRecall" => $this->getAttribute(($context["entidad"] ?? null), "id", array()))), "html", null, true);
                echo "\">
                            <input type=\"hidden\" name=\"codigos\" value=\"";
                // line 102
                echo twig_escape_filter($this->env, twig_jsonencode_filter(($context["codigos"] ?? null)), "html", null, true);
                echo "\">
                            <div class=\"panel-body\">
                                <button type=\"submit\" class=\"btn btn-primary waves-effect waves-light\">Actualizar Códigos</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        ";
            }
            // line 111
            echo "    ";
        }
        // line 112
        echo "
";
    }

    // line 115
    public function block_js($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "AdminBundle:Default:subirCodigosRecall.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  220 => 115,  215 => 112,  212 => 111,  200 => 102,  196 => 101,  188 => 95,  186 => 94,  173 => 83,  163 => 80,  160 => 79,  155 => 78,  135 => 60,  132 => 59,  102 => 32,  94 => 26,  92 => 25,  87 => 22,  81 => 19,  77 => 17,  75 => 16,  72 => 15,  66 => 13,  63 => 12,  60 => 11,  54 => 8,  48 => 7,  43 => 6,  40 => 5,  34 => 4,  30 => 1,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AdminBundle:Default:subirCodigosRecall.html.twig", "/home/120818.cloudwaysapps.com/vqpqagqqyk/public_html/recall/proyect/src/AdminBundle/Resources/views/Default/subirCodigosRecall.html.twig");
    }
}
