<?php

/* base_admin.html.twig */
class __TwigTemplate_52f225b182cb3ba69fce9fe0cfe2582c9dcdf2f7cab7a46235ccaae6caee8f3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'js' => array($this, 'block_js'),
            'ready' => array($this, 'block_ready'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\" />
    <title>Dashboard Fuso Colombia</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" />
    <meta content=\"CMS para el proyecto Recall de Fuso Colombia.\" name=\"description\" />
    <meta content=\"Coderthemes\" name=\"author\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />

    <link rel=\"shortcut icon\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/favicon.png"), "html", null, true);
        echo "\">

    <!--venobox lightbox-->
    <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/magnific-popup/dist/magnific-popup.css"), "html", null, true);
        echo "\">

    <!-- DataTables -->
    <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/datatables/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />

    <!-- Dropzone css -->
    <link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/dropzone/dist/dropzone.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">

    <!--bootstrap-wysihtml5-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"), "html", null, true);
        echo "\">
    <link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/summernote/dist/summernote.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    ";
        // line 26
        $this->displayBlock('css', $context, $blocks);
        // line 29
        echo "
    <link href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/sweetalert/dist/sweetalert.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">

    <link href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/core.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/icons.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/components.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/pages.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/menu.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/responsive.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">


    <script src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/modernizr.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        var nuevaImagen = '';
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js') }}/1.3.0/respond.min.js\"></script>
    <![endif]-->


</head>


<body class=\"fixed-left\">

<!-- Begin page -->
<div id=\"wrapper\">

    <!-- Top Bar Start -->
    <div class=\"topbar\">
        <!-- LOGO -->
        <div class=\"topbar-left\">
            <div class=\"text-center\">
                <a href=\"";
        // line 67
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_homepage");
        echo "\" class=\"logo\"><i class=\"md md-domain text-primary\" style=\"color: slategrey;\"></i> <span>Dashboard </span></a>
            </div>
        </div>
        <!-- Button mobile view to collapse sidebar menu -->
        <div class=\"navbar navbar-default\" role=\"navigation\">
            <div class=\"container\">
                <div class=\"\">
                    <div class=\"pull-left\">
                        <button type=\"button\" class=\"button-menu-mobile open-left\">
                            <i class=\"fa fa-bars\"></i>
                        </button>
                        <span class=\"clearfix\"></span>
                    </div>
                    <!--<form class=\"navbar-form pull-left\" role=\"search\">
                        <div class=\"form-group\">
                            <input type=\"text\" class=\"form-control search-bar\" placeholder=\"Type here for search...\">
                        </div>
                        <button type=\"submit\" class=\"btn btn-search\"><i class=\"fa fa-search\"></i></button>
                    </form>-->

                    <ul class=\"nav navbar-nav navbar-right pull-right\">
                        <li class=\"hidden-xs\">
                            <a href=\"#\" id=\"btn-fullscreen\" class=\"waves-effect\"><i class=\"md md-crop-free\"></i></a>
                        </li>
                        <li class=\"hidden-xs\">
                            <a href=\"";
        // line 92
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("logout");
        echo "\" class=\"waves-effect\"><i class=\"md md-exit-to-app\"></i></a>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->

    <div class=\"left side-menu\">
        <div class=\"sidebar-inner slimscrollleft\">
            <div class=\"user-details\">
                <div class=\"pull-left\">
                    <img src=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../assets/img/common/logo.png"), "html", null, true);
        echo "\" alt=\"\" class=\"thumb-md\">
                </div>
                <div class=\"user-info\">
                    <p class=\"text-muted\"><b>";
        // line 112
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute($this->getAttribute(($context["app"] ?? null), "user", array()), "username", array())), "html", null, true);
        echo "</b></p>
                    <p class=\"text-muted m-0\">Rol: ";
        // line 113
        echo twig_escape_filter($this->env, twig_lower_filter($this->env, twig_replace_filter($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? null), "user", array()), "roles", array()), 0, array(), "array"), array("ROLE_" => ""))), "html", null, true);
        echo "</p>
                </div>
            </div>
            <!--- Divider -->
            <div id=\"sidebar-menu\">
                <ul>
                    <li>
                        <a href=\"";
        // line 120
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_homepage");
        echo "\" class=\"waves-effect\"><i class=\"md md-home\"></i><span> Inicio </span></a>
                    </li>

                    ";
        // line 123
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_RECALLS")) {
            // line 124
            echo "                    <li>
                        <a href=\"";
            // line 125
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_index");
            echo "\" class=\"waves-effect\"><i class=\"md md-web\"></i><span> Recall </span></a>
                    </li>

                    <li>
                        <a href=\"";
            // line 129
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_registros");
            echo "\" class=\"waves-effect\"><i class=\"md md-list\"></i><span> Registros </span></a>
                    </li>
                    ";
        }
        // line 132
        echo "
                    ";
        // line 138
        echo "                </ul>
                <div class=\"clearfix\"></div>
            </div>
            <div class=\"clearfix\"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class=\"content-page\">
        <!-- Start content -->
        <div class=\"content\">
            <div class=\"container\">

                <!-- Page-Title -->
                <div class=\"row\">
                    <div class=\"col-sm-12\">
                        <h4 class=\"pull-left page-title\">";
        // line 159
        $this->displayBlock('pagina', $context, $blocks);
        echo "</h4>
                        <ol class=\"breadcrumb pull-right\">
                            <li><a href=\"";
        // line 161
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_homepage");
        echo "\">Inicio</a></li>
                            ";
        // line 162
        $this->displayBlock('ubicacion', $context, $blocks);
        // line 164
        echo "                        </ol>
                    </div>
                </div>


            </div>
            ";
        // line 170
        $this->displayBlock('content', $context, $blocks);
        // line 172
        echo "        </div> <!-- content -->

        ";
        // line 174
        $this->displayBlock('footer', $context, $blocks);
        // line 179
        echo "
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->

<script>
    var resizefunc = [];
</script>

<!-- Main  -->
<script src=\"";
        // line 194
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 195
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/detect.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 197
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/fastclick.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.slimscroll.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.blockUI.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 200
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/waves.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/wow.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 202
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.nicescroll.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.scrollTo.min.js"), "html", null, true);
        echo "\"></script>

<!-- Datatables-->
<script src=\"";
        // line 206
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/datatables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 207
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/datatables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>


<!-- Datatable init js -->
<script src=\"";
        // line 211
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/pages/datatables.init.js"), "html", null, true);
        echo "\"></script>


<script src=\"";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.app.js"), "html", null, true);
        echo "\"></script>



<script type=\"text/javascript\" src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"), "html", null, true);
        echo "\"></script>

<!--form validation init-->
<script src=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/summernote/dist/summernote.min.js"), "html", null, true);
        echo "\"></script>

<!-- Page Specific JS Libraries -->
<script src=\"";
        // line 225
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/dropzone/dist/dropzone.js"), "html", null, true);
        echo "\"></script>

<!-- Galeria -->
<script type=\"text/javascript\" src=\"";
        // line 228
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/isotope/dist/isotope.pkgd.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 229
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"), "html", null, true);
        echo "\"></script>

<!-- Sweet-Alert  -->
<script src=\"";
        // line 232
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/sweetalert/dist/sweetalert.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 233
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/pages/jquery.sweet-alert.init.js"), "html", null, true);
        echo "\"></script>


";
        // line 236
        $this->displayBlock('js', $context, $blocks);
        // line 238
        echo "
<script type=\"text/javascript\">
    \$(document).ready(function() {
        \$('#datatable').dataTable();
        \$('.summernote').summernote({
            height: 200,                 // set editor height

            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor

            focus: true                 // set focus to editable area after initializing summernote
        });
        ";
        // line 250
        $this->displayBlock('ready', $context, $blocks);
        // line 252
        echo "    } );
</script>

</body>
</html>";
    }

    // line 26
    public function block_css($context, array $blocks = array())
    {
        // line 27
        echo "        <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/notifications/notification.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    ";
    }

    // line 159
    public function block_pagina($context, array $blocks = array())
    {
    }

    // line 162
    public function block_ubicacion($context, array $blocks = array())
    {
        // line 163
        echo "                            ";
    }

    // line 170
    public function block_content($context, array $blocks = array())
    {
        // line 171
        echo "            ";
    }

    // line 174
    public function block_footer($context, array $blocks = array())
    {
        // line 175
        echo "            <footer class=\"footer text-right\">
                2017 © Fuso Colombia.
            </footer>
        ";
    }

    // line 236
    public function block_js($context, array $blocks = array())
    {
    }

    // line 250
    public function block_ready($context, array $blocks = array())
    {
        // line 251
        echo "        ";
    }

    public function getTemplateName()
    {
        return "base_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  483 => 251,  480 => 250,  475 => 236,  468 => 175,  465 => 174,  461 => 171,  458 => 170,  454 => 163,  451 => 162,  446 => 159,  439 => 27,  436 => 26,  428 => 252,  426 => 250,  412 => 238,  410 => 236,  404 => 233,  400 => 232,  394 => 229,  390 => 228,  384 => 225,  378 => 222,  372 => 219,  368 => 218,  361 => 214,  355 => 211,  348 => 207,  344 => 206,  338 => 203,  334 => 202,  330 => 201,  326 => 200,  322 => 199,  318 => 198,  314 => 197,  310 => 196,  306 => 195,  302 => 194,  285 => 179,  283 => 174,  279 => 172,  277 => 170,  269 => 164,  267 => 162,  263 => 161,  258 => 159,  235 => 138,  232 => 132,  226 => 129,  219 => 125,  216 => 124,  214 => 123,  208 => 120,  198 => 113,  194 => 112,  188 => 109,  168 => 92,  140 => 67,  111 => 41,  105 => 38,  101 => 37,  97 => 36,  93 => 35,  89 => 34,  85 => 33,  81 => 32,  76 => 30,  73 => 29,  71 => 26,  66 => 24,  62 => 23,  56 => 20,  50 => 17,  44 => 14,  38 => 11,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base_admin.html.twig", "/home/120818.cloudwaysapps.com/vqpqagqqyk/public_html/recall/proyect/app/Resources/views/base_admin.html.twig");
    }
}
