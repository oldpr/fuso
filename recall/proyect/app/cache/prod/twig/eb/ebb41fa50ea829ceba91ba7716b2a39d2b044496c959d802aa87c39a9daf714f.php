<?php

/* AppBundle:Default:index.html.twig */
class __TwigTemplate_f5c38053ec8d4950972017762bded314a0494ef5cb9ada39a2d18fc98f1c9b58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
    <h3>LLAMADOS DE SEGURIDAD</h3>

    <p>Bienvenido a la sección especializada de Freightliner Colombia donde se le informará si su vehículo aplica para alguna de las campañas de seguridad vigentes.</p>

    <form action=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("buscarCodigo");
        echo "\" method=\"post\">
        <input type=\"text\" name=\"codigo\" class=\"form-control\" placeholder=\"Ingrese los últimos 7 caracteres de su código VIN\" required>
        <input type=\"submit\" class=\"btn btn-negro form-control\" value=\"Buscar\">
        <div class=\"vin\">
            <a href=\"javascript: void(0);\" onmouseover=\"document.getElementById('tarjeta').style.display = 'inline';\" onmouseout=\"document.getElementById('tarjeta').style.display = 'none';\" target=\"_blank\" class=\"tooltip1\">¿Qué es un código VIN? <img src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/icon-1.png"), "html", null, true);
        echo "\"></a>
            <span id=\"tarjeta\" class=\"tooltiptext\"><img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/tarjeta.jpg"), "html", null, true);
        echo "\"></span>
        </div>

    </form>


";
    }

    public function getTemplateName()
    {
        return "AppBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 14,  45 => 13,  38 => 9,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:Default:index.html.twig", "/home/120818.cloudwaysapps.com/vqpqagqqyk/public_html/recall/proyect/src/AppBundle/Resources/views/Default/index.html.twig");
    }
}
