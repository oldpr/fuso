<?php

/* AdminBundle:Default:index.html.twig */
class __TwigTemplate_567baa1d37ff385ab4aeb65fa39ddb76cb63092a7e611d329dd7000bfa12c52d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", "AdminBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pagina($context, array $blocks = array())
    {
        // line 4
        echo "    Inicio
";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-6 col-lg-3\">
                    <a href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_index");
        echo "\">
                        <div class=\"mini-stat clearfix bx-shadow bg-white\">
                            <span class=\"mini-stat-icon bg-info\"><i class=\"md md-web\"></i></span>
                            <div class=\"mini-stat-info text-right text-dark\">
                                <span class=\"text-dark\">Recall</span>
                            </div>
                        </div>
                    </a>
            </div>

        </div>
    </div> <!-- container -->
";
    }

    public function getTemplateName()
    {
        return "AdminBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 11,  40 => 8,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AdminBundle:Default:index.html.twig", "/home/120818.cloudwaysapps.com/vqpqagqqyk/public_html/recall/proyect/src/AdminBundle/Resources/views/Default/index.html.twig");
    }
}
