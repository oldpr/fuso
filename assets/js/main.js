// GLOBAL
var log = function(obj) {
    console.log(obj);
};


$( document ).ready(function() {

    var sudoSlider = $("#slider").sudoSlider({
        effect: "fade",
        pause: 3000,
        controlsFade: false,
        auto:false,
        speed: 1000,
        numeric:true
    });



    $( "#menu-produc" ).hover(
        function() {
            $( "#effect-red" ).hide();

            $("#effect").animate({
                width: 'toggle'
            });

        }, function() {

        }
    );
    jQuery('body > div > div > div.container-recall.col-md-11 > div.contenido > div > p').each(function() {
        var text = jQuery(this).text();
        jQuery(this).text(text.replace('Freightliner Colombia', 'FUSO'));  
    });


    $( "#menu-red-distri" ).hover(
        function() {
            $( "#effect" ).hide();

            $("#effect-red").animate({
                width: 'toggle'
            });

        }, function() {

        }
    );


    $( ".container-slider,.li-postventa,.li-contacto" )
        .mouseenter(function() {
            console.log("Entro");
            $( "#effect" ).hide();
            $( "#effect-red" ).hide();
        })
        .mouseleave(function() {
            console.log("Salio")
        });

    //




    $( ".sub-red-produc" ).mouseleave(function() {
        console.log("leave");
    });


    $( ".navbar-toggle" ).on( "click", function() {


        var attrdisplay=$("#menumovil").css("display");

        if(attrdisplay=='none'){

            $( "#menumovil" ).show( "drop" );
        }else{
            $( "#menumovil" ).hide();
        }


    });

    function sendForm(event){
        var $errorMessage = $(".alert");
        // var $successMessage = $(".form-success-message");
        var url = location.origin + "/include/save-form.php";

        var $form = $("#form-comments");
        $errorMessage.hide();
        //$successMessage.hide();

        var validFormcontact=isValidForm($form);



        if(validFormcontact==1){
            $errorMessage.html('Ingrese su nombre.');
            $errorMessage.show();

        } else if(validFormcontact==2){
            $errorMessage.html('Ingrese su celular.');
            $errorMessage.show();

        }else if(validFormcontact==3){
            $errorMessage.html('Ingrese su email.');
            $errorMessage.show();

        }else if(validFormcontact==4){
            $errorMessage.html('Ingrese un email válido.');
            $errorMessage.show();

        } else if(validFormcontact==5){
            $errorMessage.html('Ingrese su ciudad.');
            $errorMessage.show();

        } else if(validFormcontact==6){
            $errorMessage.html('Seleccione un vehiculo de su interés.');
            $errorMessage.show();

        }else if(validFormcontact==7){
            $errorMessage.html('Seleccione la carrocería se su intéres.');
            $errorMessage.show();

        } else if(validFormcontact==8){
            $errorMessage.html('Debe aceptar términos y condiciones.');
            $errorMessage.show();

        } else if(validFormcontact==9){
            $errorMessage.html('Enviando datos...');
            $errorMessage.show();

            var data = $form.serialize();
            console.log("Click enviar");lbReload('thank-you-page-click','','','');
            $.ajax(url, {
                method:"POST",
                data: data,
                beforeSend:function(){},
                success:function(res){
                    if(res.status == "ok"){
                        console.log("tomalo")
                        $errorMessage.html('Datos enviados correctamente.');
                        $errorMessage.show();
                    }



                    cleanFormData($form);
                },
                complete:function(res){},
                error:function(res){
                    console.log(res);
                }
            });


        } else{
            $errorMessage.html('ha ocurrido algún error de conexión vuelva a intentarlo más tarde.');
            $errorMessage.show();

        }

        // alert(validFormcontact)



    }


    function isValidForm($form){
        var $name = $form.find("input[name='nombre']");
        var $celular = $form.find("input[name='celular']");
        var $email =$form.find("input[name='email']");
        var $ciudad = $form.find("select[name='ciudad']");
        var $vehiculointeres= $form.find("select[name='vehiculointeres']");
        var $carroceria= $form.find("select[name='carroceria']");
        var $tyc=$form.find("input[name='tyc']:checked");


        if($name.val() == ""  ){
            return 1;
        }else if($celular.val() == ""){

            return 2;
        }else if($email.val() == ""){

            return 3;
        }else if (!isValidEmailAddress($email.val())){
            return 4;
        }else if($ciudad.val() == 0){

            return 5;
        }else if($vehiculointeres.val() == 0){

            return 6;
        }else if($carroceria.val() == 0){

            return 7;
        }else if($tyc.val() == undefined){

            return 8;
        }

        return 9;
    }

    function cleanFormData($form){
        var $name = $form.find("input[name='nombre']");
        var $celular = $form.find("input[name='celular']");
        var $email =$form.find("input[name='email']");
        var $ciudad = $form.find("input[name='ciudad']");
        var $vehiculointeres= $form.find("select[name='vehiculointeres']");
        var $carroceria= $form.find("select[name='carroceria']");
        var $tyc=$form.find("input[name='tyc']:checked");

        $name.val("");
        $celular.val("");
        $email.val("");
        $ciudad.val("");
        $vehiculointeres.val("");
        $carroceria.val("");

        $("input[name='tyc']:checked").attr('checked',false);
        // $('#form-comments')[0].reset();
    }



    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        return pattern.test(emailAddress);
    }

    $( ".btn-send" ).on( "click", function() {

        sendForm();
    });


});





