<div class="container-contacto col-md-11 ">
    <div class="col-md-12 img-contacto">
        <img src="<?php echo $site_url; ?>assets/img/common/test-drive.jpg" class="img-responsive">
    </div>
    <div class="row col-md-8 col-md-offset-2 content-contacto">

        <form id="form-comments" class="form-container form-horizontal">
            <div class="form-group">

                <div class="col-sm-10">
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                </div>
            </div>
            <div class="form-group">

                <div class="col-sm-10">
                    <input type="text" class="form-control" id="celular" name="celular" placeholder="Celular" required>
                </div>
            </div>
            <div class="form-group">

                <div class="col-sm-10">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" required>
                </div>
            </div>
            <div class="form-group">

                <div class="col-sm-10">
                    <select class="selectpicker" id="ciudad" name="ciudad" data-style="btn-danger" required>
                        <option value="0">En qué ciudad queda su concesionario FUSO más cercano</option>
                        <option value="Bogotá">Bogotá</option>
                        <option value="Barranquilla">Barranquilla</option>
                        <option value="Boyacá">Boyacá</option>
                        <option value="Bucaramanga">Bucaramanga</option>
                        <option value="Cali">Cali</option>
                        <option value="Cúcuta">Cúcuta</option>
                        <option value="Ibagué">Ibagué</option>
                        <option value="Medellín">Medellín</option>
                        <option value="Montería">Montería</option>
                        <option value="Pereira">Pereira</option>
                        <option value="Villavicencio">Villavicencio</option>
                    </select>
                </div>
            </div>

            <div class="form-group">

                <div class="col-sm-10">
                    <select class="selectpicker" id="vehiculointeres" name="vehiculointeres" data-style="btn-danger" required>
                        <option value="0">Vehículo de su interés</option>

                        <option value="Canter  7.5L">Canter 8.2</option>
                        <option value="FI 10.4">FI 10.4</option>
                        <option value="FA 9.0">FA 9.0</option>
                    </select>
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-10">
                    <div class="alert">Ingresa tu nombre</div>
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label><input type="checkbox" id="tyc" name="tyc"> Acepto términos y condiciones
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label class="tyc-ver"> <a href="http://www.fuso.com.co/assets/pdf/Terminos-y-Condiciones-site-FUSO.pdf" target="_blank"> Ver  términos y condiciones  </a></label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="btn-enviar col-sm-10">
                    <div  class="btn-send btn-default ">¡Agendese ahora!</div>
                </div>
            </div>
        </form>
    </div>