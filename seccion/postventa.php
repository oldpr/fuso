	
       
       
        <div class="container-postventa col-md-11">
              
              <div class="row">
                  <div class="col-md-12">
                     <div class="tittle">
                     CONOZCA NUESTRAS OFERTAS DE REPUESTOS GENUINOS
                     </div>
                  </div>
              </div>
<!--               <div class="row">
                  <div class="col-md-12">
                    <div class="container-det-top">
                      <div class="sub-tittle">
                       Por qué usar repuestos genuinos
                      </div>
                    </div>
                  </div>
              </div> -->
              
              <div class="row">
                <div class="kit col-md-3">
                  <div class="sub-tittle aling-center">
                    Empaque Culata Canter B<br>
                      <div class="texto-posventa">REF: ME013300</div>
                  </div>
                  <div class="img aling-center">
                  <img src="<?php echo $site_url; ?>assets/img/postventa/ref1.jpg">
                     <div class="texto-posventa">
                      Precio venta público sugerido:<br> 
                      <strong>$ 78.537 no incluye IVA</strong><br>
                      APLICA PARA:<br> 
                      FUSO CANTER EURO II<br>
            
                      
                    </div>
                  </div>
                </div>

                <div class="kit col-md-3">
                  <div class="sub-tittle aling-center">
                    Juego Piñón & Corona (37/6)<br>
                      <div class="texto-posventa">REF: MC863589</div>
                  </div>
                  <div class="img aling-center">
                  <img src="<?php echo $site_url; ?>assets/img/postventa/ref2.jpg">
                     <div class="texto-posventa">
                      Precio venta público sugerido:<br> 
                      <strong>$ 3.716.259 no incluye IVA</strong><br>
                      APLICA PARA:<br> 
                      FUSO Canter FE85PG EURO II<br>
 
                      
                    </div>
                  </div>
                 

              </div> 

              <div class="row">
                  <div class="col-md-12">
                     <div class="tittle">
                     KITS DE REPUESTOS
                     </div>
                  </div>
              </div>
             <!--  <div class="row">
                  <div class="col-md-12">
                    <div class="container-det-top">
                      <div class="sub-tittle">
                       Kit de repuestos y ofertas individuales
                      </div>
                    </div>
                  </div>
              </div> -->

              <div class="row">
                <div class="kit col-md-3">
                  <div class="sub-tittle aling-center">
                    Kit Filtración FUSO Euro II<br>
                      <div class="texto-posventa">Ref: KIT FUSO FILT EII</div>
                  </div>
                  <div class="img aling-center">
                  <img src="<?php echo $site_url; ?>assets/img/postventa/ref4.jpg">
                     <div class="texto-posventa">
                      Precio venta público sugerido:<br> 
                      <strong>$ 135.534 no incluye IVA </strong><br>
                      APLICA PARA:<br> 
                      CANTER 7.5 - 8.2 EURO II<br>
                      
                      INCLUYE:<br>
                      - 1 Filtro De Aceite<br>
                      - 1 Filtro Combustible<br>
                      -1 Filtro De Aire.
                    </div>
                  </div>
                </div>
                 

                <div class="kit col-md-3">
                  <div class="sub-tittle aling-center">
                    Kit inyección FUSO Euro II<br>
                      <div class="texto-posventa">Ref: KIT FUSO INY EII</div>
                  </div>
                  <div class="img aling-center">
                  <img src="<?php echo $site_url; ?>assets/img/postventa/ref7.jpg">
                     <div class="texto-posventa">
                      Precio venta público sugerido:<br> 
                      <strong>$ 1.571.854 no incluye IVA </strong><br>
                      APLICA PARA:<br> 
                      CANTER 7.5 - 8.2 EURO II<br>
                      INCLUYE:<br>
                      - 4 Inyector Motor Canter FE649/59<br>
                      <br>
                    </div>
                  </div>
                </div>


                <div class="kit col-md-3">
                  <div class="sub-tittle aling-center">
                    Kit FUSO Suspensión Trasera<br>
                      <div class="texto-posventa">REF: KIT FUSO SUSP TRAS</div>
                  </div>
                  <div class="img aling-center">
                  <img src="<?php echo $site_url; ?>assets/img/postventa/ref5.jpg">
                     <div class="texto-posventa">
                      Precio venta público sugerido:<br> 
                      <strong>$ 372.281 no incluye IVA </strong> <br>
                      APLICA PARA:<br> 
                      CANTER 7.5 - 8.2 EURO II<br>
                      INCLUYE:<br>
                      - 2 Amortiguadores Traseros<br>
                      <br>
                      <br>
                    </div>
                  </div>
                </div>


                <div class="kit col-md-3">
                  <div class="sub-tittle aling-center">
                    Kit FUSO Suspensión Delantera <br>
                      <div class="texto-posventa">REF: KIT FUSO SUSP DEL</div>
                  </div>
                  <div class="img aling-center">
                  <img src="<?php echo $site_url; ?>assets/img/postventa/ref5.jpg">
                     <div class="texto-posventa">
                      Precio venta público sugerido:<br> 
                      <strong>$ 297.413 no incluye IVA </strong><br>
                      APLICA PARA:<br> 
                      CANTER 7.5 - 8.2 EURO II<br>
                      
                      INCLUYE:<br>
                      - 2 Amortiguadores Delanteros<br>
                      <br>
                      <br>
                    </div>
                  </div>
                </div>

              <div class="row">
                <div class="kit col-md-3">
                  <div class="sub-tittle aling-center">
                    Kit FUSO Tobera Euro II<br>
                      <div class="texto-posventa">Ref: KIT FUSO TOB EII</div>
                  </div>
                  <div class="img aling-center">
                  <img src="<?php echo $site_url; ?>assets/img/postventa/ref6.jpg">
                     <div class="texto-posventa">
                      Precio venta público sugerido:<br> 
                      <strong>$ 645.896 no incluye IVA</strong><br>
                      APLICA PARA:<br> 
                      CANTER 7.5 - 8.2 EURO II<br>

                      INCLUYE:<br>
                      - 4 Toberas de Inyección Canter<br><br>
                    </div>
                  </div>
                </div>

                <div class="kit col-md-3">
                  <div class="sub-tittle aling-center">
                    Kit Filtración FUSO Euro IV<br>
                      <div class="texto-posventa">Ref: KIT FUSO FILT EIV</div>
                  </div>
                  <div class="img aling-center">
                  <img src="<?php echo $site_url; ?>assets/img/postventa/ref4.jpg">
                     <div class="texto-posventa">
                      Precio venta público sugerido:<br> 
                      <strong>$ 207.584 no incluye IVA </strong><br>
                      APLICA PARA:<br> 
                      CANTER 7.5 - 8.2 EURO IV <br>

                      
                      INCLUYE:<br>
                      - 1 Filtro De Aceite <br>
                      - 1 Filtro Combustible<br>
                      1 Filtro De Aire           
                    </div>
                  </div>
                </div>


                <div class="kit col-md-3">
                  <div class="sub-tittle aling-center">
                    Kit Inyectores FUSO Euro IV<br>
                      <div class="texto-posventa">Ref: KIT INY FUSO EIV</div>
                  </div>
                  <div class="img aling-center">
                  <img src="<?php echo $site_url; ?>assets/img/postventa/ref8.jpg">
                     <div class="texto-posventa">
                      Precio venta público sugerido:<br> 
                      <strong>$ 5.439.513 no incluye IVA</strong> <br>
                      APLICA PARA:<br> 
                      CANTER 7.5 - 8.2 EURO IV <br>

                      INCLUYE:<br>
                      - 4 Anillo O´ring<br>
                      - 4 Inyector<br>
                      
                    </div>
                  </div>
                </div>

                <div class="kit col-md-3">
                  <div class="sub-tittle aling-center">
                    Kit Embrague <br>CANTER<br>
                      <div class="texto-posventa">REF: ME990077</div>
                  </div>
                  <div class="img aling-center">
                  <img src="<?php echo $site_url; ?>assets/img/postventa/ref3.jpg">
                     <div class="texto-posventa">
                      Precio venta público sugerido:<br> 
                      <strong>$1.198.926 no incluye IVA</strong><br>
                      APLICA PARA:<br> 
                      FUSO Canter FE85PG EURO II<br>

                      INCLUYE:<br>
                      - 4 Anillo O´ring<br>
                      - 4 Inyector<br>
                      
                    </div>
                  </div>
                </div>




               <div class="row conatner-banner">
                <div class="col-md-12">
                    <div class="banner col-md-4">
                      <img src="<?php echo $site_url; ?>assets/img/common/banner-posventa.jpg">
                    </div>

                    <div class="banner-text col-md-8">
                        <div class="text-banner">
                            Tras el cierre de la publicación de esta oferta pueden haberse producido cambios en la oferta, validar disponibilidad en la red de concesionarios autorizados de FUSO Colombia S.A.
                        </div>
                    </div>
                </div>
              </div>


        </div>
