<?php
include_once 'include/noticias.php';

$idNoticia = '';
if(isset($_GET['detproduct'])){
    $idNoticia=$_GET['detproduct'];
}
foreach($noticias as $item){
    if($item['id']==$idNoticia){
        $noticia = $item;
    }
}
?>
<div class="container-general col-md-11 ">
    <div class="navegacion row">
        <div class="anterior col-sm-6">
            <a href="<?php echo $link->ToSubSeccion('noticia', $noticia['id-anterior']); ?>" >Anterior noticia</a>
        </div>
        <div class="siguiente col-sm-6">
            <a href="<?php echo $link->ToSubSeccion('noticia', $noticia['id-siguiente']); ?>" >Siguiente noticia</a>
        </div>
    </div>
    <div class="noticia">
        <div class="imagen" style="background: url(<?php echo $site_url.$noticia['imagen']; ?>) no-repeat center; background-size: cover"></div>
        <span class="fecha"><?php echo $noticia['fecha']; ?></span>
        <h3 class="titulo"><?php echo $noticia['titulo']; ?></h3>
        <p>
            <?php echo $noticia['texto']; ?>
        </p>
    </div>
</div>