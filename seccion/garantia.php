<div class="container-garantia col-md-11 ">

    <div class="row col-md-8 col-md-offset-0">
        <div class="cap1">
            <h3>TÉRMINOS DE COBERTURA</h3>

        </div>
        <div class="tablecont">

            <h3>CONDICIONES GENERALES</h3>
            <p style="font-size: 15px; text-align: left;">Lagarantía otorgada por Daimler Colombia S.Acubre la reparación o cambio de las piezas o componentes originales del vehículo que presenten defectos de calidad o ensamble de fábrica, sin costo alguno para el propietario salvo el de los componentes que sufren desgaste o deterioro por el uso normal del vehículo (Ver componentes no cubiertos).</p>
            <p style="font-size: 13px; text-align: left;">Todos los campos de esta tabla se deben diligenciar para activar este certificado. El concesionario debe completar cada uno de los datos requeridos.</p>
            <h3>CONDICIONES ESPECIALES</h3>
            <p>Daimler Colombia S.A. podrásustituir o reparar los componentes o piezas reconocidas como defectuosas o mal ensambladas de fábrica en el vehículo.Los gastos de mano de obra para la remoción e instalación de los componentes, conjuntos o sistemas del vehículo por garantía, también serán cubiertos por la misma, siempre que tales trabajos se realicen en los talleres autorizados por Daimler Colombia, de acuerdo con el listado del presente documento, que se actualizará periódicamente en la página web <a href="http://www.daimler.com.co">www.daimler.com.co</a>.</p>
            <p>El propietario del vehículo debe acreditar la realización de los servicios de conservación y mantenimiento, señalando los mismos en el Cuaderno de Mantenimiento  e Instrucciones de Servicio que se aconseja sean realizados prioritariamente en un taller autorizado por Daimler Colombia S.A.</p>
            <p>En las reparaciones de garantía, por causas imputables a Daimler Colombia  S.A. y/o a uno de los  concesionarios autorizados, se interrumpiráautomáticamente el plazo de la garantía y dicho tiempo se computará como prolongación de la misma.</p>
            <p>No están incluidas dentro de la garantía las consecuencias indirectas de un posible defecto tales como las pérdidas comerciales, indemnizaciones por el tiempo de inmovilización,lucro cesante, gasolina, teléfonos, viajes,gastos por alquiler de un vehículo, alojamientos, comidas, almacenaje, etc.</p>
            <h3>EXCLUSIONES DE LA GARANTIA</h3>
            <p>La garantía perderá  su validez cuando ocurra cualquiera de los siguientes casos:</p>
            <p>Si no se realizan los servicios de conservación, revisiones y mantenimiento obligatorios en los períodos señalados en el Cuaderno de Mantenimiento entregado al propietario siempre y cuando el evento reportado por el cliente esté relacionado directa o indirectamente a la falta o calidad del mantenimiento realizado.</p>
            <p>Cuando se han producido daños en el vehículo por  no seguir las observaciones técnicas de Daimler Colombia, relacionadas con (a.) el uso o manejo inadecuado del vehículo, o (b.) servicios de conservación y mantenimiento preventivos inadecuados y/o no efectuados de acuerdo a las condiciones de uso equivalentes (normales, mixtas y difíciles), así como por lo citado en el Cuaderno de Mantenimiento y el evento reportado esté relacionado con estas condiciones incumplidas.</p>
            <p>Si los daños o mal funcionamiento del vehículo se deben a uso  inadecuado (ej. sobrepeso,sobrecupo,uso inadecuado en caminos irregulares y no pavimentados), accidentes, vandalismo, condiciones ambientales adversas, desastres naturales y en general por caso fortuito o fuerza mayor.</p>
            <p>Si el vehículo ha sido utilizado en condiciones y propósitos distintos a  las normales para los cuales fue diseñado o no ha sido operado siguiendo las recomendaciones del Manual de Operación y el Cuaderno de Mantenimiento  entregados junto con el vehículo y  las demás instrucciones comunicadas por Daimler Colombia S.A.</p>
            <p>Si la instalación de carrocerías, blindajes, equipos de volteo, furgones, tanques adicionales, accesorios, equipo electrónico,  alarmas u otros componentes afectan el buen funcionamiento u ocasionen daños en el vehículo.</p>
            <p>Si el vehículo ha sido alterado, modificado o reparado por personas distintas al personal calificado de los talleres autorizados o si se instalan en el vehículo,repuestos o accesorios no autorizados por Daimler Colombia S.A. y dicha alteración tiene relación directa con la falla reportada.</p>
            <p>Si fuesen constatadas violaciones o alteraciones al velocímetro, odómetro (cuenta kilómetros)  o si se observara que el mismo ha permanecido fuera de funcionamiento con el fin de obtener el beneficio de garantía.</p>
            <p>Elementos o piezas que hayan sido intervenidas o reparadas por colisión, ya que estas estarán cubiertas por la garantía de piezas o reparación.De igual forma no tendrán garantía los vehículos declarados en pérdida total o que sean salvamento.</p>
            <p>Daños en tapices y revestimientos internos relacionados con desgaste por el uso normal de estas piezas, al igual que deformaciones, decoloraciones y manchas ocasionadas por excesiva exposición al sol después de 12 meses o 20.000 Km (lo primero que ocurra).</p>
            <p>Ajustes al vehículo: estos trabajos por no involucrar cambio de piezas (entonación, ajuste de suspensión, ajuste de frenos, etc) no son cubiertos por la garantía, ya que se generan a consecuencia del uso normal del vehículo, Los ruidos y rechinidos que no sean consecuencia de un daño o desajuste mecánico tendrán cobertura hasta los 12 meses o 20.000 km (lo primero que ocurra).</p>
            <p>Corrosión en el vehículo será cubierta hasta los 6 meses sin límite de kilometraje. Esta cobertura ofrece garantía contra la corrosión de cualquier metal o aleación metálica que forme parte del vehículo.
                No serán cubiertas corrosiones graves causadas por utilización de soluciones inadecuadas en el lavado, alta presión en el lavado, detergentes, etc. De igual forma, corrosiones causadas por sales corrosivas de carretera y corrosiones generadas por modificaciones en la cabina son excluidas de la garantía.
            </p>
            <h3>MANTENIMIENTO</h3>
            <p>La presente garantía no cubre costos de mantenimiento de acuerdo a lo establecido en los manuales de operación y conservación del vehículo, o  fallas debidas al uso de combustible, lubricantes o refrigerantes que no cumplan con las especificaciones recomendadas por Daimler Colombia S.A.</p>
            <p>La realización de los mantenimientos establecidos en el cuaderno de mantenimiento y el uso de combustible, aceite, lubricantes, y refrigerantes apropiados es  responsabilidad  del propietario.</p>
            <h3>PERIODO DE COBERTURA</h3>
            <p>Esta garantía aplica para el primer propietario y los dueños subsiguientes y estará vigente durante el periodo o el recorrido mencionados a continuación, contados desde la fecha de entrega del vehículo al primer propietario (estipulada en este certificado), y  terminará cuando se cumpla el plazo o alcance el kilometraje indicado, lo primero que ocurra y aplica para los vehículos nuevos, importados o ensamblados por <strong> DaimlerColombia S.A,</strong> vendidos por la compañía o por sus concesionarios autorizados, matriculados y usados en Colombia.</p>
            <p>&nbsp;</p>
            <table class="table tabled tablebold">
                <tbody>
                <tr>
                    <td colspan="2">&nbsp;COBERTURA GARANTÍA CAMIONES FUSO</td>
                </tr>
                <tr>
                    <td>&nbsp;Modelo</td>
                    <td>Tiempo&nbsp;</td>
                    <td>Kilometraje&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;Canter FE84, FE85, FE85Max.</td>
                    <td>&nbsp;2 años</td>
                    <td>&nbsp;50.000 Km</td>
                </tr>
                <tr>
                    <td>&nbsp;Canter 2 7.5M, 7.5L, 8.2</td>
                    <td>&nbsp;3 años</td>
                    <td>&nbsp;100.000 Km</td>
                </tr>
                <tr>
                    <td>&nbsp;FA, FI, FJ</td>
                    <td>&nbsp;3 años</td>
                    <td>&nbsp;100.000 Km</td>
                </tr>
                </tbody>
            </table>

            <p>La garantía de la batería original es de 12 meses sin límite de kilometraje</p>
            <p>Todas las intervenciones realizadas al amparo de la Garantía Contractual están garantizadas hasta el vencimiento del período original de garantía del vehículo (tiempo o kilometraje). Para las reparaciones realizadas a partir de la finalización del mes 24 o repuestos comprados por mostrador aplica la siguiente vigencia.</p>
            <p>&nbsp;</p>
            <table class="table tabled tablebold">
                <tbody>
                <tr style="height: 49px;">
                    <td style="height: 49px;" colspan="2">VIGENCIA GARANTÍA FUSO</td>
                </tr>
                <tr style="height: 49px;">
                    <td style="height: 49px;">Instalado en taller</td>
                    <td style="height: 49px;">Vendido por mostrador</td>
                </tr>
                <tr style="height: 49px;">
                    <td style="height: 49px;">6 meses sin límite de Kilometraje</td>
                    <td style="height: 49px;">3 meses</td>
                </tr>
                <tr style="height: 49px;">
                    <td style="height: 49px;" colspan="2">El plazo y/o kilometraje se contabilizará, tomando como base la fecha y/o kilometraje registrado en la Factura</td>
                </tr>
                </tbody>
            </table>
            <p>&nbsp;</p>

            <h3>PIEZAS O COMPONENTES NO CUBIERTOS POR LA &nbsp;GARANTÍA</h3>
            <p>La presente garantía excluye componentes y piezas que presenten desgaste normalo que sean de mantenimiento como: filtros (lubricación, admisión de aire, combustible, sistema neumático y sistema de aire acondicionado y calefacción), líquidos de llenado (lubricantes, combustible, refrigerante, líquido de frenos, fluidos hidráulicos, fluidos del sistema de aire acondicionado, grasas), bujías, correas, soportes de motor, embrague (disco y prensa), retenedores, guayas y cables, guardapolvos, rótulas, bujes y terminalesde la suspensión, amortiguadores, muelles, ballestas, fuelles neumáticos, discos, bandas, campanas y pastillas de freno, fusibles, relés, bombillos, cables eléctricos del trailer, plumillas, vidrios y herramienta, Pintura al interior de guardabarros, Pintura en la superficie trasera de la cabina. Las piezas o sistemas antes mencionados podrán ser cubiertos en los casos que se verifique que un defecto de fabricación o ensamble de la pieza es la causa del evento reportado por el cliente.</p>
            <p>La garantía tampoco cubre alineación y balanceo de ruedas, Mano de obra, ni el mantenimiento o reajuste de las partes  anteriormente mencionadas y otras que estén dentro del mismo concepto de desgaste normal o de mantenimiento.</p>
            <p>Los neumáticos gozan de la garantía que otorgan sus fabricantes acogiéndose a las condiciones otorgadas por los mismos. En todo caso,  las llantas estarían exentas en caso que el daño sea consecuencia de golpes, pellizcamientos causados por golpes, desgastes prematuros por falta o fallas en la alineación y el uso de una presión de inflado incorrecta. En caso de cualquier reclamación por Garantía, el concesionario de su preferencia está a disposición para encaminar su solicitud al representante local de la marca de los neumáticos de su vehículo.</p>
            <h3>PROCEDIMIENTO PARA HACER EFECTIVA LA GARANTIA</h3>
            <p>El Propietario podrá solicitar que se haga efectiva la garantía en cualquiera de los talleres o concesionarios autorizados en el territorio colombiano publicados en nuestra página <a href="http://www.daimler.com.co">www.daimler.com.co</a>.</p>
            <h3>RESPONSABILIDAD</h3>
            <p>Daimler Colombia S.A. y las plantas fabricantes de los vehículos vendidos por la Compañía o por sus concesionarios autorizados del país, se reservan el derecho de modificar y/o mejorar en cualquier momento  las especificaciones de sus productos y el cubrimiento de la garantía, sin que por ello contraigan la obligación de efectuar tales modificaciones o perfeccionamientos en sus productos vendidos anteriormente.</p>
            <p>Daimler Colombia S.A. tiene un mecanismo institucional de recepción ytrámite de Peticiones, Quejas y Reclamos (PQR), si usted lo requierepuede acudir al Coordinador de Customer Care, quien le explicara nuestroprocedimiento interno de PQR. Los datos de contacto son: Coordinador deCustomer Care, tel. 4236700 ext. 1642 y/o a los teléfonos (1) 742 6850 y el celular es 3162801318de Bogotá. La presentación de PQR´S No tiene que ser personal, nirequiere de intervención de abogado.En caso de persistir la inconformidad, le recordamos que puede acudirante las autoridades competentes</p>
            <p>Declaro que he recibido, leído completamente, comprendido y aceptado los  términos, las condiciones de la garantía y las demás informaciones contenidas en éste documento.</p>

        </div>

    </div>
</div>