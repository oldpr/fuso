

<?php
include_once 'include/products.php';



if(isset($_GET['detproduct'])){
    $producto=$_GET['detproduct'];
}



// Este ciclo muestra todas las claves del array asociativo
// donde el valor equivale a "manzana"
//print_r($products[0]["id"]);
$img="";
$name="";
$pic_product="";
$pic_cart="";
$Descripcion="";
$motor="";
$peso="";
$largo="";
$fichatecnica="";


foreach($products as $item)
{
    //foreach($item as $key => $value)
    //{
    //echo $item['id']."---".$producto."</br>" ;


    if($item['id']==$producto){
        $img=$item['pic'];
        $name=$item['name'];
        $pic_product=$item['pic-product'];
        $pic_cart=$item['pic'];
        $Descripcion=$item['description'];
        $motor=$item['motor'];
        $peso=$item['pesos'];
        $largo=$item['largo'];
        $fichatecnica=$item['ficha'];

    }

}



?>    <div class="container-productos-detalles col-md-11">

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="container-det-top">
                <div class="tittle-detail-product">
                    <h1 class="titulo-grande-n"><?php echo $name; ?></h1>
                </div>
                <div class="sub-canter">
                    <h2 class="subtitulo-grande-n"><?php echo $pic_product; ?></h2>
                </div>
                <div class="cart-canter">
                    <img src="<?php echo $site_url.$pic_cart; ?>" class="img-responsive">
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="descrip-canter">
                <div class="tittle">
                    Descripción
                </div>
                <div class="descri-pr">
                    <?php echo $Descripcion;?>
                </div>
            </div>

            <div class="ficha clearfix">

                <div class="motor col-md-offset-0 col-md-4 col-sm-4 col-xs-12 col-xs-offset-0">

                    <?php echo $motor; ?>
                </div>

                <div class="motor col-md-offset-0 col-md-4 col-sm-4 col-xs-12 col-xs-offset-0">

                    <?php echo $peso; ?>
                </div>

                <!--<div class="motor col-md-offset-0 col-md-4 col-sm-4 col-xs-12 col-xs-offset-0">
                        <img src="<?php echo $site_url; ?>assets/img/producto/icon-potencia.png">
                        <div class="detail-motor">
                          <div class="title" >
                            MOTOR
                          </div>
                          <div class="detai ">
                            Cilindrada: 4.899 cm3 <br>
                            Potencia: 177HP @ 2.700 rpm<br>
                            Torque: 54Kg-m @ 1.600 rpm<br>
                          </div>
                        </div>
                      </div>-->
                <div class="motor col-md-offset-0 col-md-4 col-sm-4 col-xs-12 col-xs-offset-0">

                    <?php echo $largo; ?>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="enlaces-detail-pro col-md-12">
                <div class="col-md-6 aling-right"><a href="<?php echo $fichatecnica; ?>" target="_blank">Descargar la ficha técnica</a>   </div>
                <div class="col-md-6 aling-left"><a  href="<?php echo $link->ToSeccion('contacto'); ?>" >Solicite una prueba de producto</a></div>
            </div>
        </div>
        <div class="row">
            <div class="legal-detail-produc col-md-10 col-md-offset-1">
                Las  ilustraciones muestran equipos opcionales y accesorios no pertenecientes al equipamiento de serie. Pueden producirse cambios o modificaciones en el diseño y/o equipamiento del producto, los cuales sugerimos verificar en nuestra red de concesionarios autorizados. Para mayor información sobre el portafolio de productos, equipamiento y precio visite la red de concesionarios autorizados de Daimler Colombia S.A.  *Capacidad de carga bruta = Peso bruto Vehicular - Peso Chasis  **Este vehículo cuenta con una garantía de 3 años o 100.000 Kilómetros lo que primero ocurra.
            </div>
        </div>
    </div>
</div>