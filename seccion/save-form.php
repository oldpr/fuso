<?php

error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
ini_set('display_errors',1);
header('Content-Type: application/json');

$name = utf8_decode(strip_tags($_POST['nombre']));
$celular = utf8_decode(strip_tags($_POST['celular']));
$mail = utf8_decode(strip_tags($_POST['email']));
$ciudad = utf8_decode(strip_tags($_POST['ciudad']));
$vehiculointeres = utf8_decode(strip_tags($_POST['vehiculointeres']));
$carroceria = (isset($_POST['carroceria']))?utf8_decode(strip_tags($_POST['carroceria'])):'';


if (empty($name) || empty($mail) || empty($celular) || !filter_var($mail, FILTER_VALIDATE_EMAIL)) {
    echo json_encode([
        "status" => "error",
        "code" => 200,
        "message" => "invalid data",
    ]);
    exit();
}

$to = "diana.montes@daimler.com";
$from = "contacto-fuso@fuso.com.co";
$subject = "Fuso contacto";
$headers = getHeaders($from);
$html = getHTMLBody([
    "title" => "Fuso",
    "name" => $name,
    "mail" => $mail,
    "comment" => $comment,
]);
if (!mail($to, $subject, $html, $headers)) {
    echo json_encode([
        "status" => "error",
        "code" => 200,
        "message" => "invalid data",
    ]);
    exit();
}

echo json_encode([
    "status" => "ok",
    "code" => 100,
    "message" => "mail sent",
]);

function getHeaders($from, $reply = "") {
    $headers = "From: " . strip_tags($from) . "\r\n";
    $headers .= "Reply-To: ". strip_tags($reply) . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    return $headers;
}

function getHTMLBody($data) {
    $message = '<html lang="es"><body>';
    $message .= '<h1>'.$data["title"].'</h1>';
    $message .= '<h3>Nombre: '.$data["name"].'</h3>';
    $message .= '<h3>Celular: '.$data["celular"].'</h3>';
    $message .= '<h3>Email: '.$data["email"].'</h3>';
    $message .= '<h3>Ciudad: '.$data["ciudad"].'</h3>';
    $message .= '</body></html>';

    return $message;
}
