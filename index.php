<?php
date_default_timezone_set('America/Bogota');


include('class/class_link.php');
//include('class/class_object.php');

$link = new Link();
$site_url = $link->Build('');
$name_page='';

//$object = new Object();

$seccion = '';
$name_page="Home";


include('templates/layout-star.php');

if(isset($_GET['seccion'])){

 $seccion = $_GET['seccion'];

}

if($seccion != ''){

	if($seccion == 'productos'){
		include 'seccion/productos.php';
	}

	if($seccion == 'posventa'){
		include 'seccion/postventa.php';
	}

	if($seccion == 'contacto'){
		include 'seccion/contacto.php';
	}

	if($seccion == 'test-drive'){
		include 'seccion/test-drive.php';
	}

	if($seccion == 'productos-detalles'){
		include 'seccion/productos-detalles.php';
	}
	if($seccion == 'red-de-distribucion'){
		include 'seccion/red-de-distribucion.php';
	}
	if($seccion == 'save-form'){
		include 'seccion/save-form.php';
	}

    if($seccion == 'garantia'){
        include 'seccion/garantia.php';
    }

    if($seccion == 'recall'){
        include 'seccion/recall.php';
    }

    if($seccion == 'noticias'){
        include 'seccion/noticias.php';
    }

    if($seccion == 'noticia'){
        include 'seccion/noticia.php';
    }

	

}else{

	include 'seccion/home.php';
}


include('templates/layout-end.php');